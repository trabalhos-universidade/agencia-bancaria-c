#include "funcoesmap.h"

int menuprincipal();
void header();
int menurelatorio();

int main()
{
    int op, principal_cliente, principal_agencia, principal_conta;
    int cons_agencia, cons_cliente, edita_agencia;
    int cons_conta, edita_cliente,edita_conta,op_relatorio,rel_agencia,rel_cliente,rel_conta;

    //cria uma ponteiro para maptodas
    mapagencia *agen;
    agen = new mapagencia;
    mapcliente *clien;
    clien = new mapcliente;
    mapconta *cont;
    cont = new mapconta; 
    
    do
    {
        system("cls");
        header();
        
        //recebe op��o do usuario no menu principal
        op = menuprincipal();    
        
        switch(op)
        {  // 1 - op��es agencias     
           case 1: do
                   {
                         system("cls");
                         header();
                         
                         //recebe op��o do usuario do menu principal do cliente   
                         principal_agencia = agen->menuagencia();
                         
                         switch(principal_agencia)
                         {
                            case 1: system("cls");
                                    header();
                                    agen->carregar();
                                    system("pause");
                            break;
                            
                            case 2: system("cls");
                                    header();
                                    agen->grava();
                                    system("pause");
                            break;
                            
                            case 3: system("cls");
                                    header();
                                    agen->cadagencia();
                                    system("pause");
                            break;
                         
                            case 4: system("cls");
                                    header();
                                    agen->removeagencia();
                                    system("pause");
                            break;
                            
                            case 5: do
                                    {
                                       system("cls");
                                       header();
                                    
                                       edita_agencia = agen->menueditaagencia();
                                       
                                       switch(edita_agencia)
                                       {
                                         case 1: system("cls");
                                                 header();                                                 
                                                 agen->editabanco();
                                                 system("pause");
                                         break;
                                         
                                         case 2: system("cls");
                                                 header();                                                 
                                                 agen->editaendereco();
                                                 system("pause");
                                         break;
                                         
                                         case 3: system("cls");
                                                 header();                                                 
                                                 agen->editacidade();
                                                 system("pause");
                                         break;
                                         
                                         case 4: system("cls");
                                                 header();                                                 
                                                 agen->editabairro();
                                                 system("pause");
                                         break;
                                          
                                         case 5: system("cls");
                                                 header();                                                 
                                                 agen->editaestado();
                                                 system("pause");
                                         break;
                                         
                                         case 6: system("cls");
                                                 header();                                                 
                                                 agen->editacep();
                                                 system("pause");
                                         break;
                                         
                                         case 7: system("cls");
                                                 header();                                                 
                                                 agen->editatelefone();
                                                 system("pause");
                                         break;
                                         
                                         case 8: system("cls");
                                                 header();                                                 
                                                 agen->editatodas();
                                                 system("pause");
                                         break;
                                       }
                                    }while(edita_agencia != 0);
                            break;
                            
                            case 6: do
                                    {
                                       system("cls");
                                       header();
                                       
                                       cons_agencia = agen->menuagenciaconsulta();
                                       
                                       switch(cons_agencia)
                                       {
                                         case 1: system("cls");
                                                 header();
                                                 agen->mostracodigo();
                                                 system("pause");
                                         break; 
                                         
                                         case 2: system("cls");
                                                 header();
                                                 agen->mostrabanco();
                                                 system("pause");
                                         break;
                                         
                                         case 3: system("cls");
                                                 header();
                                                 agen->mostraendereco();
                                                 system("pause");
                                         break;
                                         
                                         case 4: system("cls");
                                                 header();
                                                 agen->mostracidade();
                                                 system("pause");
                                         break;
                                         
                                         case 5: system("cls");
                                                 header();
                                                 agen->mostrabairro();
                                                 system("pause");
                                         break;
                                         
                                         case 6: system("cls");
                                                 header();
                                                 agen->mostraestado();
                                                 system("pause");
                                         break;
                                         
                                         case 7: system("cls");
                                                 header();
                                                 agen->mostracep();
                                                 system("pause");
                                         break;
                                         
                                         case 8: system("cls");
                                                 header();
                                                 agen->mostratelefone();
                                                 system("pause");
                                         break;
                                         
                                         case 9: system("cls");
                                                 header();
                                                 agen->mostratodas();
                                                 system("pause");
                                         break;
                                       }                                      
                                    }while(cons_agencia != 0);
                            break;
                            
                            case 7: do
                                    {
                                      system("cls");
                                      header();
                                      
                                      rel_agencia = agen->menuagenciarel();
                                      
                                      switch(rel_agencia)
                                      {
                                         case 1: //Gera relatorio de todas as agencias cadastradas
                                                 system("cls");
                                                 header();
                                                 
                                                 agen->gerarelatoriogeral();
                                                 system("pause");
                                         break;
                                         
                                         case 2: //gera relatorio de todos os bancos
                                                 system("cls");
                                                 header();
                                                 
                                                 agen->relatoriobanco();
                                                 system("pause");
                                         break;                                      
                                         
                                         case 3: system("cls");
                                                 header();
                                                 
                                                 agen->relatoriocidade();
                                                 system("pause");
                                         break;
                                         
                                         case 4: system("cls");
                                                 header();
                                                 
                                                 agen->relatoriobairro();
                                                 system("pause");
                                         break;
                                         
                                         case 5: system("cls");
                                                 header();
                                                 
                                                 agen->relatorioestado();
                                                 system("pause");
                                         break;
                                      }
                                    }while(rel_agencia != 0);
                            break;
                         }
                         
                   }while(principal_agencia != 0);
           break;
           
           // 2 - op��es clientes
           case 2: do
                   {
                         system("cls");
                         header();
                         
                         //recebe op��o do usuario do menu principal do cliente   
                         principal_cliente = clien->menucliente();
                         
                         switch(principal_cliente)
                         {                                                  
                            case 1: system("cls");
                                    header();
                                    clien->carregar();
                                    system("pause");
                            break;
                            
                            case 2: system("cls");
                                    header();
                                    clien->grava();
                                    system("pause");
                            break;
                                                    
                            case 3: system("cls");
                                    header();
                                    clien->cadcliente();
                                    system("pause");
                            break;
                         
                            case 4: system("cls");
                                    header();
                                    clien->removecliente();
                                    system("pause");
                            break;
                            
                            case 5: do
                                    {
                                       system("cls");
                                       header();
                                    
                                       edita_cliente = clien->menueditacliente();
                                       
                                       switch(edita_cliente)
                                       {
                                         case 1: system("cls");
                                                 header();                                                 
                                                 clien->editanome();
                                                 system("pause");
                                         break;
                                         
                                         case 2: system("cls");
                                                 header();                                                 
                                                 clien->editacpf();
                                                 system("pause");
                                         break;
                                         
                                         case 3: system("cls");
                                                 header();                                                 
                                                 clien->editarg();
                                                 system("pause");
                                         break;
                                         
                                         case 4: system("cls");
                                                 header();                                                 
                                                 clien->editasexo();
                                                 system("pause");
                                         break;
                                         
                                         case 5: system("cls");
                                                 header();                                                 
                                                 clien->editaendereco();
                                                 system("pause");
                                         break;
                                         
                                         case 6: system("cls");
                                                 header();                                                 
                                                 clien->editabairro();
                                                 system("pause");
                                         break;
                                         
                                         case 7: system("cls");
                                                 header();                                                 
                                                 clien->editacidade();
                                                 system("pause");
                                         break;
                                         
                                         case 8: system("cls");
                                                 header();                                                 
                                                 clien->editaestado();
                                                 system("pause");
                                         break;
                                         
                                         case 9: system("cls");
                                                 header();                                                 
                                                 clien->editacep();
                                                 system("pause");
                                         break;
                                         
                                         case 10: system("cls");
                                                 header();                                                 
                                                 clien->editatelefone();
                                                 system("pause");
                                         break;                                         
                                         
                                         case 11: system("cls");
                                                 header();                                                 
                                                 clien->editaestado_civil();
                                                 system("pause");
                                         break;
                                         
                                         case 12: system("cls");
                                                 header();                                                 
                                                 clien->editatodas();
                                                 system("pause");
                                         break;                                       
                                       }
                                    }while(edita_cliente != 0);
                            break;
                            
                            case 6: do
                                    {
                                      system("cls");                            
                                      header();
                                      
                                      cons_cliente = clien->menuclienteconsulta();
                                      
                                      switch(cons_cliente)
                                      {
                                        case 1: system("cls");
                                                 header();
                                                 clien->mostracodigo();
                                                 system("pause");
                                        break;
                                        
                                        case 2: system("cls");
                                                 header();
                                                 clien->mostranome();
                                                 system("pause");
                                        break;
                                        
                                        case 3: system("cls");
                                                 header();
                                                 clien->mostracpf();
                                                 system("pause");
                                        break;
                                        
                                        case 4: system("cls");
                                                 header();
                                                 clien->mostrarg();
                                                 system("pause");
                                        break;
                                        
                                        case 5: system("cls");
                                                 header();
                                                 clien->mostrasexo();
                                                 system("pause");
                                        break;
                                        
                                        case 6: system("cls");
                                                 header();
                                                 clien->mostraendereco();
                                                 system("pause");
                                        break;
                                        
                                        case 7: system("cls");
                                                 header();
                                                 clien->mostrabairro();
                                                 system("pause");
                                        break;
                                        
                                        case 8: system("cls");
                                                 header();
                                                 clien->mostracidade();
                                                 system("pause");
                                        break;
                                        
                                        case 9: system("cls");
                                                 header();
                                                 clien->mostraestado();
                                                 system("pause");
                                        break;
                                        
                                        case 10: system("cls");
                                                 header();
                                                 clien->mostracep();
                                                 system("pause");
                                        break;  
                                        
                                        case 11: system("cls");
                                                 header();
                                                 clien->mostratelefone();
                                                 system("pause");
                                        break;
                                        
                                        case 12: system("cls");
                                                 header();
                                                 clien->mostraestado_civil();
                                                 system("pause");
                                        break;
                                        
                                        case 13: system("cls");
                                                 header();
                                                 clien->mostratodas();
                                                 system("pause");                                                 
                                                 
                                        break;
                                      }
                                    }while(cons_cliente != 0);
                            break;
                            
                            case 7: do
                                    {
                                      system("cls");
                                      header();
                                      
                                      rel_cliente = clien->menuclienterel();
                                      
                                      switch(rel_cliente)
                                      {
                                         case 1: //Gera relatorio de todos os clientes cadastrados
                                                 system("cls");
                                                 header();
                                                 
                                                 clien->gerarelatoriogeral();
                                                 system("pause");
                                         break;
                                         
                                         case 2: //gera relatorio de todos os nomes de clientes
                                                 system("cls");
                                                 header();
                                                 
                                                 clien->relatorionome();
                                                 system("pause");
                                         break;                                      
                                         
                                         case 3: //gera relatorio sexo
                                                 system("cls");
                                                 header();
                                                 
                                                 clien->relatoriosexo();
                                                 system("pause");
                                         break;
                                         
                                         case 4: //gera relatorio endere�o
                                                 system("cls");
                                                 header();
                                                 
                                                 clien->relatorioendereco();
                                                 system("pause");
                                         break;
                                         
                                         case 5: //gera relatorio bairro
                                                 system("cls");
                                                 header();
                                                 
                                                 clien->relatoriobairro();
                                                 system("pause");
                                         break;
                                         
                                         case 6: //gera relatorio cidade
                                                 system("cls");
                                                 header();
                                                 
                                                 clien->relatoriocidade();
                                                 system("pause");
                                         break;
                                         
                                         case 7: //gera relatorio estado
                                                 system("cls");
                                                 header();
                                                 
                                                 clien->relatorioestado();
                                                 system("pause");
                                         break;
                                         
                                         case 8: //gera relatorio cep
                                                 system("cls");
                                                 header();
                                                 
                                                 clien->relatoriocep();
                                                 system("pause");
                                         break;
                                         
                                         case 9: //gera relatorio estado civil
                                                 system("cls");
                                                 header();
                                                 
                                                 clien->relatorioestadocivil();
                                                 system("pause");
                                         break;
                                      }
                                    }while(rel_cliente != 0);
                            break;
                         }
                         
                   }while(principal_cliente != 0);
           break;
           
           // 3 - op��es contas bancarias
           case 3:
                do
                   {
                         system("cls");
                         header();
                         
                         //recebe op��o do usuario do menu principal da conta
                         principal_conta = cont->menuconta();
                         
                         switch(principal_conta)
                         {
                            case 1: //carrega base
                                    system("cls");
                                    header();
                                    cont->carregar();
                                    system("pause");
                            break;
                            
                            case 2: //grava base
                                    system("cls");
                                    header();
                                    cont->grava();
                                    system("pause");
                            break;
                                                        
                            case 3: //cadastra conta bancaria
                                    system("cls");
                                    header();
				                    cont->aberturaconta(*clien, *agen);                            
                                    system("pause");
                            break;
                         
                            case 4: //remove conta bancaria
                                    system("cls");
                                    header();
                                    cont->removeconta();
                                    system("pause");
                            break;
                            
                            case 5: //edita = = entra em outro sub-menu que tem 6 op��es
				                    do
                                    {
                                       system("cls");
                                       header();
                                     
				                       //pega op��o do menueditaconta

                                       edita_conta = cont->menueditaconta();
                                       
                                       switch(edita_conta)
                                       {                                                                     
                                         case 1: system("cls");
                                                 header();                                                 
                                                 cont->editatipo();
                                                 system("pause");
                                         break;                                                                         
                                         
                                         case 2: system("cls");
                                                 header();                                                 
                                                 cont->editadatacadastro();
                                                 system("pause");
                                         break;
                                         
                                         case 3: system("cls");
                                                 header();                                                 
                                                 cont->editaatendente();
                                                 system("pause");
                                         break;                                         
                                                               
                                                                                                                        
                                       }
                                    }while(edita_conta != 0);

                            break;
                            
                            case 6: do
                                    {
                                      system("cls");
                                      header();
                                      
                                      cons_conta = cont->menucontaconsulta();
                                      
                                      switch(cons_conta)
                                      {
                                        case 1: system("cls");
                                                header();                                                
                                                cont->mostracodigo(*clien);
                                                system("pause");
                                        break;
                                        
                                        case 2: //mostracliente
                                                system("cls");
                                                header();
                                                cont->mostracliente(*clien);
                                                system("pause");
                                        break;
                                        
                                        case 3: //mostra agencia
                                                system("cls");
                                                header();
                                                cont->mostraagencia(*clien, *agen);
                                                system("pause");
                                        break;
                                        
                                        case 4: system("cls");
                                                header();
                                                cont->mostratipo(*clien);
                                                system("pause");
                                        break;                                       
                                        
                                        case 5: system("cls");
                                                header();
                                                cont->mostradatacadastro(*clien);
                                                system("pause");
                                        break;
                                        
                                        case 6: system("cls");
                                                header();
                                                cont->mostraatendente(*clien);
                                                system("pause");
                                        break;
                                        
                                        case 7:  system("cls");
                                                 header();
                                                 cont->mostratodas(*clien);
                                                 system("pause");
                                        break;
                                      }
                                      
                                    }while(cons_conta != 0);
                            break;
                            
                            case 7: do
                                    {
                                      system("cls");
                                      header();
                                      
                                      rel_conta = cont->menucontarel();
                                      
                                      switch(rel_conta)
                                      {
                                        case 1: //gera relatorio
                                                system("cls");
                                                header();
                                                                                            
                                                cont->gerarelatoriogeral(*clien);
                                                system("pause");                                                
                                                
                                        break;
                                        
                                        case 2: //gera relatorio por tipo
                                                system("cls");
                                                header();
                                                
                                                cont->relatoriotipo(*clien);
                                                system("pause");
                                        break;
                                        
                                        case 3: //gera relatorio data cadastro
                                                system("cls");
                                                header();
                                                
                                                cont->relatoriodatacadastro(*clien);
                                                system("pause");
                                        break;
                                        
                                        case 4: //gera relatorio atendente
                                                system("cls");
                                                header();
                                                
                                                cont->relatorioatendente(*clien);
                                                system("pause");
                                        break;
                                      }
                                    }while(rel_conta != 0);
                            break;
                         }
                         
                   }while(principal_conta != 0);
           break;
           
           case 4: //relatorios                
                  do
                  {
                     system("cls");
                     header();
                   
                     op_relatorio = menurelatorio();
                   
                     cout<<"Relatorio Geral";                     
                                      
                     switch(op_relatorio)
                     {
                        case 1: //relatorio geral agencia
                                system("cls");
                                header();                                
                                cout<<"  principal -> relatorios -> TELA/ARQUIVO \n\n";
                                char op;
                                cout<<"  RELATORIO EM TELA OU ARQUIVO? (T - Tela / A - Arquivo): ";
                                cin>>op;
                                if((op == 'T') || (op == 't'))
                                {
                                   agen->mostratodas();
                                   system("pause");
                                }
                                else if((op == 'A') || (op == 'a'))
                                {
                                   agen->gerarelatoriogeral();
                                   system("pause");
                                } 
                        break;
                                        
                        case 2: //relatorio geral cliente
                                system("cls");
                                header();                                
                                cout<<"  principal -> relatorios -> TELA/ARQUIVO \n\n";
                                char op1;
                                cout<<"  RELATORIO EM TELA OU ARQUIVO? (T - Tela / A - Arquivo): ";
                                cin>>op1;
                                if((op1 == 'T') || (op1 == 't'))
                                {
                                   clien->mostratodas();
                                   system("pause");
                                }
                                else if((op1 == 'A') || (op1 == 'a'))
                                {
                                   clien->gerarelatoriogeral();
                                   system("pause");
                                } 
                         break;
                         
                         case 3: //relatorio geral contas bancarias
                                 system("cls");
                                 header();
                                 cout<<"  principal -> relatorios -> TELA/ARQUIVO \n\n";
                                 char op2;
                                 cout<<"  RELATORIO EM TELA OU ARQUIVO? (T - Tela / A - Arquivo): ";
                                 cin>>op2;
                                 if((op2 == 'T') || (op2 == 't'))
                                 {
                                    cont->mostratodas(*clien);
                                    system("pause");
                                 }
                                 else if((op2 == 'A') || (op2 == 'a'))
                                 {
                                    cont->gerarelatoriogeral(*clien);
                                    system("pause");
                                 } 
                         break;                                 
                      }
                   }while(op_relatorio != 0);                   
           break;
        }
    }while (op != 0); //fim do while do menuprincipal   
    
}

int menuprincipal()
{
    int opcao; 
      //  "$ =============================================================== $\n"
    cout<<endl;
    cout<<"  #-#-#-#-#-#-#-#      MENU PRINCIPAL             #-#-#-#-#-#-#-#  \n";   
    cout<<"  #-#-#-#-#-#-#-#    (1) - AGENCIAS               #-#-#-#-#-#-#-#  \n";
    cout<<"  #-#-#-#-#-#-#-#    (2) - CLIENTES               #-#-#-#-#-#-#-#  \n";
    cout<<"  #-#-#-#-#-#-#-#    (3) - CONTAS BANCARIAS       #-#-#-#-#-#-#-#  \n";
    cout<<"  #-#-#-#-#-#-#-#    (4) - RELATORIOS             #-#-#-#-#-#-#-#  \n";
    cout<<"  #-#-#-#-#-#-#-#    (0) - SAIR DO SISTEMA        #-#-#-#-#-#-#-#  \n";
    cout<<"  ===============================================================   \n"; 
    cout<<"  Informe a opcao desejada: ";    
    cin>>opcao;   
    return opcao;
}
int menurelatorio()
{
    int opcao;  
    //    "$ =============================================================== $\n"    
    cout<<"  principal -> RELATORIOS                                          \n";
    cout<<endl;
    cout<<"  #-#-#-#-#-#-#-#      RELATORIO GERAL            #-#-#-#-#-#-#-#  \n";   
    cout<<"  #-#-#-#-#-#-#-#    (1) - AGENCIAS               #-#-#-#-#-#-#-#  \n";
    cout<<"  #-#-#-#-#-#-#-#    (2) - CLIENTES               #-#-#-#-#-#-#-#  \n";
    cout<<"  #-#-#-#-#-#-#-#    (3) - CONTAS BANCARIAS       #-#-#-#-#-#-#-#  \n";    
    cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
    cout<<"  ===============================================================   \n"; 
    cout<<"  Informe a opcao desejada: ";    
    cin>>opcao;   
    return opcao;
}

void header()
{
    cout<<"$ =============================================================== $\n";
    cout<<"  ---------------------------------------------------------------  \n";
    cout<<"  ################ SISTEMA DE ABERTURA DE CONTAS ################  \n";
    cout<<endl;
    cout<<"  ###### Atendentes: Fabio Duarte de Souza \n";
    cout<<"                     Fabio Feltrin \n";
    cout<<"  ===============================================================   \n";
    
}    


