#include "funcoes.h"

/*
  Classe mapagencia
*/

class mapagencia
{
      private:
              map<int, agencia> mapag;
              
      public:        
             bool vazio();
             void gerarelatoriogeral();
             void relatoriobanco();
             void relatoriocidade();
             void relatoriobairro();
             void relatorioestado();
             void grava();
             void salvanabase();
             void carregardabase();
             void carregar();
             int menuagencia();
             int menuagenciaconsulta();
             int menuagenciarel();
             int menueditaagencia();
             void editabanco();
             void editaendereco();
             void editacidade();
             void editabairro();
             void editaestado();
             void editacep();
             void editatelefone();
             void editatodas();
             void cadagencia();
             void removeagencia();             
             void mostratodas();
             void mostracodigo();
             void mostrabanco();
             void mostraendereco();
             void mostracidade();
             void mostrabairro();
             void mostraestado();
             void mostracep();
             void mostratelefone();
             bool existeagencia(int numagencia);
             
             //prototipo das fun��es da clase mapagencia
             //inserir, remover, listar, pesquisar
              
};

/*
  Classe mapcliente
*/

class mapcliente
{
      private:
              map<int, cliente> mapcli;
              
      public:
             void carregar();
             void gerarelatoriogeral();
             void relatorionome();
             void relatoriosexo();
             void relatorioendereco();
             void relatoriobairro();
             void relatoriocidade();
             void relatorioestado();
             void relatoriocep();
             void relatorioestadocivil();
             bool vazio();
             void grava();
             void salvanabase();
             void carregardabase();
             int menucliente();            
             int menuclienteconsulta();
             int menuclienterel();
             int menueditacliente();
             void mostratodas();
             void removecliente();
             void cadcliente();
             void editanome();             
             void editacpf();
             void editarg();
             void editasexo();
             void editaendereco();
             void editabairro();
             void editacidade();
             void editaestado();
             void editacep();
             void editatelefone();
             void editaestado_civil();
             void editatodas();
             void mostracodigo();
             void mostranome();
             void mostracpf();
             void mostrarg();
             void mostrasexo();
             void mostraendereco();
             void mostracidade();
             void mostrabairro();
             void mostraestado();
             void mostracep();
             void mostratelefone();
             void mostraestado_civil();
             bool existecliente(int numcliente);
             string nomecliente(int numcliente);             
             //prototipo das fun��es da classe mapcliente
             //inserir, remover, listar, pesquisar
};

/*
  Classe mapconta
*/

class mapconta
{
      private:
              map<int, conta> mapcont;             
              
      public:
             void gerarelatoriogeral(mapcliente clien);
             void relatoriotipo(mapcliente clien);
             void relatorioatendente(mapcliente clien);
             void relatoriodatacadastro(mapcliente clien);
             void carregar();
             void grava();
             void carregardabase();
             void salvanabase();
             int menuconta();
             int menucontarel();
             int menucontaconsulta();
    	     int menueditaconta();    	    
    	     void mostratodas(mapcliente clien);
    	     void editatipo();
    	     void editadatacadastro();
    	     void editaatendente();
    	     void mostracodigo(mapcliente clien);
    	     void mostratipo(mapcliente clien);
    	     void mostradatacadastro(mapcliente clien);
    	     void mostraatendente(mapcliente clien);
    	     void mostracliente(mapcliente clien);
    	     void mostraagencia(mapcliente clien, mapagencia agen);
    	     void removeconta();
    	     void aberturaconta(mapcliente clien, mapagencia agen);
    	     bool existeconta(int numconta);
			 //void total();
             //prototipos da funcao da classe mapconta
             //inserir, remover, listar, pesquisar
};
