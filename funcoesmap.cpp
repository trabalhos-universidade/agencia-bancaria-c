#include "funcoesmap.h"

/*
  Fun��es da classe mapagencias
*/

int mapagencia::menuagencia()
{
   int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> AGENCIAS                                            \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#  GERENCIAMENTO DE AGENCIAS      #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (1) - CARREGAR BASE          #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - GRAVAR BASE            #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - CADASTRAR              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - REMOVER                #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (5) - EDITAR                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (6) - CONSULTAR              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (7) - RELATORIOS             #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;
}

int mapagencia::menuagenciaconsulta()
{
    int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias -> CONSULTAR                               \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#  CONSULTAR AGENCIAS             #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (1) - CODIGO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - BANCO                  #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - ENDERECO               #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - CIDADE                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (5) - BAIRRO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (6) - ESTADO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (7) - CEP                    #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (8) - TELEFONE               #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (9) - TODAS                  #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;
}

int mapagencia::menuagenciarel()
{
    int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias ->  RELATORIOS                            \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#     RELATORIOS AGENCIAS         #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (1) - GERAL                  #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - BANCO                  #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - CIDADE                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - BAIRRO                 #-#-#-#-#-#-#-#  \n";   
   cout<<"  #-#-#-#-#-#-#-#    (5) - ESTADO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;
}

void mapagencia::carregar()
{     
     int codigo;
     fstream arq;
     string banco, endereco, cidade, bairro, estado, cep, telefone;
     agencia auxagencia;    
     arq.clear();
     mapag.clear();
     arq.open("base_agencias.txt", ios::in);
     if(!arq.fail())     
     {       
       while(!arq.eof())
       {
         //pega dados do arquivo               
         arq>>codigo>>banco>>endereco>>cidade>>bairro>>estado>>cep>>telefone; 
         auxagencia.setbanco(banco);         
         auxagencia.setendereco(endereco);
         auxagencia.setcidade(cidade);
         auxagencia.setbairro(bairro);
         auxagencia.setestado(estado);
         auxagencia.setcep(cep);
         auxagencia.settelefone(telefone);
         
         mapag.insert(make_pair(codigo, auxagencia));
                  
       }
            cout<<"  #-#-#-#-#-#-#-# AGENCIAS IMPORTADAS COM SUCESSO #-#-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
     arq.close();
     }
     else
     {
         cout<<"Arquivo nao pode ser aberto";
     }
}

bool mapagencia::vazio()
{
     if(mapag.empty())
  return true;
return false;
}

void mapagencia::grava() // gravar o arquivo
{
     map<int, agencia>::iterator aux = mapag.begin();
     fstream arq;  
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> agencias -> GRAVAR BASE                               \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  GRAVACAO DE AGENCIAS           #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapag.empty())
   {
       arq.clear();
       arq.open("base_agencias.txt", ios :: out);           
       if(!arq.fail())
       {
         for(aux = mapag.begin(); aux != mapag.end(); aux++)
         {               
           arq<< aux-> first <<"\t"<< aux -> second.getbanco()<<"\t"<< aux->second.getendereco()<<"\t"<<aux->second.getcidade()<<"\t"<<aux->second.getbairro()<<"\t"<<aux->second.getestado()<<"\t"<<aux->second.getcep()<<"\t"<<aux->second.gettelefone();
           arq<<"\n";
         }
         cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: base_agencias.txt"<<endl;
         cout<<"  Concluido...\n";         
         system("pause");
         cout<<endl;
         cout<<"  #-#-#-#-#-#-#-#  GRAVACAO REALIZADA COM SUCESSO #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
         arq.close();
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

//gera relatorio

void mapagencia::gerarelatoriogeral() // gravar o arquivo
{
     map<int, agencia>::iterator aux = mapag.begin();
     fstream arq;  
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> agencias -> relatorios -> GERAL                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE AGENCIAS           #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";
     
   if(!mapag.empty())
   {
       arq.clear();
       arq.open("relatorio_agencias.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"####################### - RELATORIO DE AGENCIAS - ########################### \n";
         arq<<"\n\n";
         for(aux = mapag.begin(); aux != mapag.end(); aux++)
         {      
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> AGENCIA: "<<aux->second.getbanco()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" - \n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";
           //arq<< aux-> first <<"\t"<< aux -> second.getbanco()<<"\t"<< aux->second.getendereco()<<"\t"<<aux->second.getcidade()<<"\t"<<aux->second.getbairro()<<"\t"<<aux->second.getestado()<<"\t"<<aux->second.getcep()<<"\t"<<aux->second.gettelefone();
               
           
         }
         cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_agencias.txt"<<endl;
         cout<<"  Concluido...\n";         
         system("pause");
         cout<<endl;
         cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
         arq.close();
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapagencia::relatoriobanco() // gravar o arquivo
{
     map<int, agencia>::iterator aux = mapag.begin();
     fstream arq;  
     string banco;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> agencias -> relatorios -> BANCO                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE AGENCIAS           #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapag.empty())
   {
       cout<<"  #-#-# -> Banco: ";
       fflush(stdin);
       getline(cin, banco);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO BANCO COM NOME "<<banco<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_banco_agencias.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE AGENCIAS POR BANCO - ##################### \n";
         arq<<"\n\n"; 
         for(aux = mapag.begin(); aux != mapag.end(); aux++)
         {  
           if(aux->second.getbanco() == banco)
           {
            n++;                                                
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> AGENCIA: "<<aux->second.getbanco()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" - \n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";           
           }       
           
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO ENCONTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_banco_agencias.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapagencia::relatoriocidade() // gravar o arquivo
{
     map<int, agencia>::iterator aux = mapag.begin();
     fstream arq;  
     string cidade;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> agencias -> relatorios -> CIDADE                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE AGENCIAS           #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapag.empty())
   {
       cout<<"  #-#-# -> Cidade: ";
       fflush(stdin);
       getline(cin, cidade);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CIDADE COM NOME "<<cidade<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_cidade_agencias.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE AGENCIAS POR CIDADE - #################### \n";
         arq<<"\n\n";
         for(aux = mapag.begin(); aux != mapag.end(); aux++)
         {  
           if(aux->second.getcidade() == cidade)
           {
            n++;                                       
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> AGENCIA: "<<aux->second.getbanco()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" - \n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";           
           }       
           
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO ENCONTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_cidade_agencias.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapagencia::relatoriobairro() // gravar o arquivo
{
     map<int, agencia>::iterator aux = mapag.begin();
     fstream arq;  
     string bairro;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> agencias -> relatorios -> BAIRRO                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE AGENCIAS           #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapag.empty())
   {
       cout<<"  #-#-# -> Bairro: ";
       fflush(stdin);
       getline(cin, bairro);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO BAIRRO COM NOME "<<bairro<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_bairro_agencias.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE AGENCIAS POR BAIRRO - #################### \n";
         arq<<"\n\n";  
                  
         for(aux = mapag.begin(); aux != mapag.end(); aux++)
         {  
           if(aux->second.getbairro() == bairro)
           {
               n++;                   
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> AGENCIA: "<<aux->second.getbanco()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" - \n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";           
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO ENCONTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_bairro_agencias.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}
void mapagencia::relatorioestado() // gravar o arquivo
{
     map<int, agencia>::iterator aux = mapag.begin();
     fstream arq;  
     string estado;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> agencias -> relatorios -> ESTADO                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE AGENCIAS           #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapag.empty())
   {
       cout<<"  #-#-# -> Estado: ";
       fflush(stdin);
       getline(cin, estado);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO ESTADO COM NOME "<<estado<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_estado_agencias.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE AGENCIAS POR ESTADO - #################### \n";
         arq<<"\n\n";  
                  
         for(aux = mapag.begin(); aux != mapag.end(); aux++)
         {  
           if(aux->second.getestado() == estado)
           {
               n++;                   
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> AGENCIA: "<<aux->second.getbanco()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" - \n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";           
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO ENCONTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_estado_agencias.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

bool mapagencia::existeagencia(int numagencia)
{
    map<int, agencia>::iterator auxag = mapag.find(numagencia);
    if (auxag == mapag.end())
        return false;
    return true;
}    

void mapagencia::salvanabase()
{     
      char opcao;
      cout<<endl;
      cout<<"  #-#-# -> Deseja salvar as alteracoes na base? (S - Sim / N - Nao): ";              
      cin>>opcao;
      if((opcao == 'S') || (opcao == 's'))
      {
         grava();
      } 
}

void mapagencia::carregardabase()
{
     char opcao;
     cout<<endl;
     cout<<"  #-#-# -> Deseja carregar informacoes da base? (S - Sim / N - Nao): ";              
     cin>>opcao;
     if((opcao == 'S') || (opcao == 's'))
     {
       carregar();
     }    
}

void mapagencia::cadagencia()
{
    pair<map<int,agencia>::iterator,bool> ret;    
    agencia auxagencia;
    int opcao, codigo;
    char op;
    string banco, endereco, cidade, bairro, estado, cep,telefone;    

    //    "$ =============================================================== $\n"    
    cout<<"  principal -> agencias -> CADASTRAR                               \n";
    cout<<endl;
    cout<<"  #-#-#-#-#-#-#-#  CADASTRO DE AGENCIAS           #-#-#-#-#-#-#-#  \n";
    cout<<"  ===============================================================  \n";
    if(mapag.empty())
    {
    carregardabase();     
    }
    cout<<"  #-#-# -> Codigo: ";
    cin>>codigo;
    cout<<endl;
    
    //faz uma busca para ver se o codigo ja existe    
    if(!existeagencia(codigo))
    {	
        //Fazer verifica��o na hora que inserir o nome do banco
        cout<<"  #-#-# -> Banco: ";
        fflush(stdin);
        getline(cin, banco);
        cout<<endl;
        cout<<"  #-#-# -> Endereco: ";
        fflush(stdin);
        getline(cin, endereco);       
        cout<<endl;
        cout<<"  #-#-# -> Cidade: ";
        fflush(stdin);
        getline(cin, cidade);
        cout<<endl;
        cout<<"  #-#-# -> Bairro: ";
        fflush(stdin);
        getline(cin, bairro);
        cout<<endl;
        cout<<"  #-#-# -> Estado: ";
        fflush(stdin);
        getline(cin, estado);
        cout<<endl;
        cout<<"  #-#-# -> CEP: ";
        fflush(stdin);
        getline(cin, cep);
        cout<<endl;
        cout<<"  #-#-# -> Telefone: ";
        fflush(stdin);
        getline(cin, telefone);	   			        
        cout<<endl;
        
        //Seta os valores
        auxagencia.setbanco(banco);
        auxagencia.setendereco(endereco);
        auxagencia.setcidade(cidade);
        auxagencia.setbairro(bairro);
        auxagencia.setestado(estado);
        auxagencia.setcep(cep);
        auxagencia.settelefone(telefone);
        
        //Adiciona
        ret = mapag.insert(make_pair(codigo, auxagencia));
        if(ret.second ==  true)
        {
            cout<<"  #-#-#-#-#-#-#-#  AGENCIA CADASTRADA COM SUCESSO #-#-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";            
            salvanabase();                                               
        }
        else
        {
            cout<<"  #-#-#-#-#-#-#-#  OCORREU ERRO NO CADASTRAMENTO  #-#-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n"; 
        }   
    }    
    else
    {
        cout<<"  #-#-#-#-#-#-#-#         CODIGO EXISTENTE        #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";

    }                            	
}

void mapagencia::removeagencia()
{
    int cod;
    //    "$ =============================================================== $\n"    
    cout<<"  principal -> agencias -> REMOVER                               \n";
    cout<<endl;
    cout<<"  #-#-#-#-#-#-#-#       REMOCAO DE AGENCIAS       #-#-#-#-#-#-#-#  \n";
    cout<<"  ===============================================================  \n";  
    cout<<endl;
    if(mapag.empty())
    {
       carregardabase();
    }
    
    if (!mapag.empty())
    {
         cout<<"  #-#-# -> Codigo: ";
         cin>>cod;
         cout<<endl;                     
         
         if(existeagencia(cod))
         {
              cout<<"  AGUARDE UM MOMENTO....REMOVENDO AGENCIA COM CODIGO "<<cod<<endl;
              system("pause");
              cout<<endl;
              mapag.erase(cod);
              cout<<"  #-#-#-#-#-#-#-#  AGENCIA REMOVIDA COM SUCESSO   #-#-#-#-#-#-#-#  \n";
              cout<<"  ===============================================================  \n";
              salvanabase();              
         }
         else
         {
              cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
              cout<<"  ===============================================================  \n"; 
         }
    }
    else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

int mapagencia::menueditaagencia()
{
   int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias -> EDITAR                                 \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#     EDITAR AGENCIAS             #-#-#-#-#-#-#-#  \n";   
   cout<<"  #-#-#-#-#-#-#-#    (1) - BANCO                  #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - ENDERECO               #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - CIDADE                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - BAIRRO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (5) - ESTADO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (6) - CEP                    #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (7) - TELEFONE               #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (8) - TODOS OS CAMPOS        #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;    
}

void mapagencia::editabanco()
{ 
   string banco;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias -> editar -> BANCO                        \n";
   cout<<endl;
   if(mapag.empty())
   {
     carregardabase();
   }
   
   if (!mapag.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeagencia(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Banco: ";
       fflush(stdin);
       getline(cin, banco);
       mapag[cod].setbanco(banco);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";   
       salvanabase();           
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
   
}

void mapagencia::editaendereco()
{ 
   string endereco;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias -> editar -> ENDERECO                      \n";
   cout<<endl;
   if(mapag.empty())
   {
     carregardabase();
   }
   
   if (!mapag.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeagencia(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Endereco: ";
       fflush(stdin);
       getline(cin, endereco);
       mapag[cod].setendereco(endereco);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";
       salvanabase();              
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
   
}

void mapagencia::editacidade()
{ 
   string cidade;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias -> editar -> CIDADE                        \n";
   cout<<endl;
   if(mapag.empty())
   {
      carregardabase();
   }
   
   if (!mapag.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeagencia(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Cidade: ";
       fflush(stdin);
       getline(cin, cidade);
       mapag[cod].setcidade(cidade);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n"; 
       salvanabase();             
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
   
}

void mapagencia::editabairro()
{ 
   string bairro;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias -> editar -> BAIRRO                        \n";
   cout<<endl;
   if(mapag.empty())
   {
     carregardabase();
   }
   
   if (!mapag.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeagencia(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Bairro: ";
       fflush(stdin);
       getline(cin, bairro);
       mapag[cod].setbairro(bairro);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n"; 
       salvanabase();             
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
   
}

void mapagencia::editaestado()
{ 
   string estado;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias -> editar -> ESTADO                        \n";
   cout<<endl;
   if(mapag.empty())
   {
      carregardabase();
   }
   
   if (!mapag.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeagencia(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Estado: ";
       fflush(stdin);
       getline(cin, estado);
       mapag[cod].setestado(estado);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";     
       salvanabase();         
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
   
}

void mapagencia::editacep()
{ 
   string cep;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias -> editar -> CEP                        \n";
   cout<<endl;
   if(mapag.empty())
   {
      carregardabase();
   }
   
   if (!mapag.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeagencia(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Cep: ";
       fflush(stdin);
       getline(cin, cep);
       mapag[cod].setcep(cep);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";      
       salvanabase();        
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
   
}

void mapagencia::editatelefone()
{ 
   string telefone;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias -> editar -> TELEFONE                      \n";
   cout<<endl;
   if(mapag.empty())
   {
     carregardabase();
   }
   
   if (!mapag.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeagencia(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Telefone: ";
       fflush(stdin);
       getline(cin, telefone);
       mapag[cod].settelefone(telefone);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n"; 
       salvanabase();             
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
   
}

void mapagencia::editatodas()
{ 
   string banco, endereco, cidade, bairro,estado,cep, telefone;
   int cod;
   agencia auxa; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> agencias -> editar -> TODOS OS CAMPOS               \n";
   cout<<endl;
   if(mapag.empty())
   {
     carregardabase();
   }
   
   if (!mapag.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeagencia(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Banco: ";
       fflush(stdin);
       getline(cin, banco);
       cout<<"  #-#-# -> Endereco: ";
       fflush(stdin);
       getline(cin, endereco);
       cout<<"  #-#-# -> Cidade: ";
       fflush(stdin);
       getline(cin, cidade);
       cout<<"  #-#-# -> Bairro: ";
       fflush(stdin);
       getline(cin, bairro);
       cout<<"  #-#-# -> Estado: ";
       fflush(stdin);
       getline(cin, estado);
       cout<<"  #-#-# -> Cep: ";
       fflush(stdin);
       getline(cin, cep);
       cout<<"  #-#-# -> Telefone: ";
       fflush(stdin);
       getline(cin, telefone);
       auxa.setbanco(banco);
       auxa.setendereco(endereco);
       auxa.setcidade(cidade);
       auxa.setbairro(bairro);
       auxa.setestado(estado);
       auxa.setcep(cep);
       auxa.settelefone(telefone);
       mapag[cod] = auxa;
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPOS EDITADOS COM SUCESSO  #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";
       salvanabase();              
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
   
}

void mapagencia::mostratodas()
{
     map<int, agencia>::iterator aux;
     if(mapag.empty())
     {
       carregardabase();
     }
     
     if(!mapag.empty())
     {
         //    "$ =============================================================== $\n"    
        cout<<"  principal -> agencias -> consultar -> TODOS                       \n";
        cout<<endl;
         for(aux = mapag.begin(); aux != mapag.end(); aux++)
         {
             aux->second.mostraagencia();
         }    
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapagencia::mostracodigo()
{
     map<int, agencia>::iterator aux;
     int cod;
      //    "$ =============================================================== $\n"    
     cout<<"  principal -> agencias -> consultar -> CODIGO                       \n";
     cout<<endl;
     if(mapag.empty())
     {
       carregardabase();
     }
     if(!mapag.empty())
     {         
        cout<<"  #-#-# -> Codigo: ";
        cin>>cod;
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<cod<<endl;
        system("pause");
        cout<<endl;
        
        aux = mapag.find(cod);
        
        if(aux != mapag.end())
        {
          cout<<"  #-#-# -> Codigo localizado."<<endl;
          aux->second.mostraagencia();         
          cout<<endl;
                      
         }
         else
         {
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapagencia::mostrabanco()
{
     map<int, agencia>::iterator aux;
     string banco;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> agencias -> consultar -> BANCO                       \n";
        cout<<endl;
     if(mapag.empty())
     {
       carregardabase();
     }
     
     if(!mapag.empty())
     {         
        cout<<"  #-#-# -> Banco: ";
        fflush(stdin);
        getline(cin, banco);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM NOME "<<banco<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapag.begin(); aux != mapag.end(); aux++)
        {
          if(aux->second.getbanco() == banco)
          {
            n++;            
            aux->second.mostraagencia();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapagencia::mostraendereco()
{
     map<int, agencia>::iterator aux;
     string endereco;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> agencias -> consultar -> ENDERECO                       \n";
        cout<<endl;
      if(mapag.empty())
     {
       carregardabase();
     }
     
     if(!mapag.empty())
     {         
        cout<<"  #-#-# -> Endereco: ";
        fflush(stdin);
        getline(cin, endereco);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM ENDERECO "<<endereco<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapag.begin(); aux != mapag.end(); aux++)
        {
          if(aux->second.getendereco() == endereco)
          {
            n++;            
            aux->second.mostraagencia();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapagencia::mostracidade()
{
     map<int, agencia>::iterator aux;
     string cidade;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> agencias -> consultar -> CIDADE                       \n";
        cout<<endl;
     if(mapag.empty())
     {
       carregardabase();
     }
     
     if(!mapag.empty())
     {         
        cout<<"  #-#-# -> Cidade: ";
        fflush(stdin);
        getline(cin, cidade);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CIDADE "<<cidade<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapag.begin(); aux != mapag.end(); aux++)
        {
          if(aux->second.getcidade() == cidade)
          {
            n++;            
            aux->second.mostraagencia();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapagencia::mostrabairro()
{
     map<int, agencia>::iterator aux;
     string bairro;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> agencias -> consultar -> BAIRRO                       \n";
        cout<<endl;
        if(mapag.empty())
     {
       carregardabase();
     }
     if(!mapag.empty())
     {         
        cout<<"  #-#-# -> Bairro: ";
        fflush(stdin);
        getline(cin, bairro);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM BAIRRO "<<bairro<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapag.begin(); aux != mapag.end(); aux++)
        {
          if(aux->second.getbairro() == bairro)
          {
            n++;            
            aux->second.mostraagencia();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapagencia::mostraestado()
{
     map<int, agencia>::iterator aux;
     string estado;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> agencias -> consultar -> ESTADO                       \n";
        cout<<endl;
        if(mapag.empty())
     {
       carregardabase();
     }
     if(!mapag.empty())
     {         
        cout<<"  #-#-# -> Estado: ";
        fflush(stdin);
        getline(cin, estado);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM ESTADO "<<estado<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapag.begin(); aux != mapag.end(); aux++)
        {
          if(aux->second.getestado() == estado)
          {
            n++;            
            aux->second.mostraagencia();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapagencia::mostracep()
{
     map<int, agencia>::iterator aux;
     string cep;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> agencias -> consultar -> CEP                       \n";
        cout<<endl;
        if(mapag.empty())
     {
       carregardabase();
     }
     if(!mapag.empty())
     {         
        cout<<"  #-#-# -> Cep: ";
        fflush(stdin);
        getline(cin, cep);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CEP "<<cep<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapag.begin(); aux != mapag.end(); aux++)
        {
          if(aux->second.getcep() == cep)
          {
            n++;            
            aux->second.mostraagencia();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapagencia::mostratelefone()
{
     map<int, agencia>::iterator aux;
     string telefone;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> agencias -> consultar -> TELEFONE                       \n";
        cout<<endl;
        if(mapag.empty())
     {
       carregardabase();
     }
     if(!mapag.empty())
     {         
        cout<<"  #-#-# -> Telefone: ";
        fflush(stdin);
        getline(cin, telefone);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM TELEFONE "<<telefone<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapag.begin(); aux != mapag.end(); aux++)
        {
          if(aux->second.gettelefone() == telefone)
          {
            n++;            
            aux->second.mostraagencia();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     AGENCIA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA AGENCIA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

/*
  Fun��es da classe mapcliente
*/

int mapcliente::menucliente()
{
   int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> CLIENTES                                            \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#  GERENCIAMENTO DE CLIENTES      #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (1) - CARREGAR BASE          #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - GRAVAR BASE            #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - CADASTRAR              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - REMOVER                #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (5) - EDITAR                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (6) - CONSULTAR              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (7) - RELATORIOS             #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;
}

int mapcliente::menuclienteconsulta()
{
     int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> CONSULTAR                               \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#      CONSULTAR CLIENTES         #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (1) - CODIGO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - NOME                   #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - CPF                    #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - RG                     #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (5) - SEXO                   #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (6) - ENDERECO               #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (7) - BAIRRO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (8) - CIDADE                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (9) - ESTADO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (10) - CEP                   #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (11) - TELEFONE              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (12) - ESTADO CIVIL          #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (13) - TODOS                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;
}

int mapcliente::menuclienterel()
{
     int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes ->  RELATORIOS                            \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#     RELATORIOS CLIENTES         #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (1) - GERAL                  #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - NOME                   #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - SEXO                   #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - ENDERECO               #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (5) - BAIRRO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (6) - CIDADE                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (7) - ESTADO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (8) - CEP                    #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (9) - ESTADO CIVIL           #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;
}

int mapcliente::menueditacliente()
{
   int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> EDITAR                                 \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#     EDITAR CLIENTES             #-#-#-#-#-#-#-#  \n";   
   cout<<"  #-#-#-#-#-#-#-#    (1) - NOME                   #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - CPF                    #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - RG                     #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - SEXO                   #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (5) - ENDERECO               #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (6) - BAIRRO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (7) - CIDADE                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (8) - ESTADO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (9) - CEP                    #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (10) - TELEFONE              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (11) - ESTADO CIVIL          #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (12) - TODOS OS CAMPOS       #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;    
}

bool mapcliente::vazio()
{
     if(mapcli.empty())
  return true;
return false;
}

void mapcliente::carregar()
{     
     int codigo;
     fstream arq;
     string nome,cpf, rg,estado_civil, endereco, cidade, bairro, estado, cep, telefone;
     char sexo;
     cliente auxcliente;    
     arq.clear();
     mapcli.clear();
     arq.open("base_clientes.txt", ios::in);
     if(!arq.fail())     
     {       
       while(!arq.eof())
       {
         //pega dados do arquivo               
         arq>>codigo>>nome>>cpf>>rg>>sexo>>endereco>>bairro>>cidade>>estado>>cep>>telefone>>estado_civil; 
         auxcliente.setnome(nome);         
         auxcliente.setcpf(cpf);
         auxcliente.setrg(rg);
         auxcliente.setsexo(sexo);
         auxcliente.setendereco(endereco);
         auxcliente.setbairro(bairro);
         auxcliente.setcidade(cidade);         
         auxcliente.setestado(estado);
         auxcliente.setcep(cep);
         auxcliente.settelefone(telefone);
         auxcliente.setestado_civil(estado_civil);
         
         mapcli.insert(make_pair(codigo, auxcliente));
                  
       }
            cout<<"  #-#-#-#-#-#-#-# CLIENTES IMPORTADOS COM SUCESSO #-#-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
     arq.close();
     }
     else
     {
         cout<<"Arquivo nao pode ser aberto";
     }
}

void mapcliente::grava() // gravar o arquivo
{
     map<int, cliente>::iterator aux = mapcli.begin();
     fstream arq;  
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> clientes -> GRAVAR BASE                               \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  GRAVACAO DE CLIENTES           #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcli.empty())
   {
       arq.clear();
       arq.open("base_clientes.txt", ios :: out);           
       if(!arq.fail())
       {
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {               
           arq<< aux-> first <<"\t"<< aux -> second.getnome()<<"\t"<< aux->second.getcpf()<<"\t"<<aux->second.getrg()<<"\t"<<aux->second.getsexo()<<"\t"<<aux->second.getendereco()<<"\t"<<aux->second.getbairro()<<"\t"<<aux->second.getcidade()<<"\t"<<aux->second.getestado()<<"\t"<<aux->second.getcep()<<"\t"<<aux->second.gettelefone()<<"\t"<<aux->second.getestado_civil();
           arq<<"\n";
         }
         cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: base_clientes.txt"<<endl;
         cout<<"  Concluido...\n";         
         system("pause");
         cout<<endl;
         cout<<"  #-#-#-#-#-#-#-#  GRAVACAO REALIZADA COM SUCESSO #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
         arq.close();
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#    NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::salvanabase()
{     
      char opcao;
      cout<<endl;
      cout<<"  #-#-# -> Deseja salvar as alteracoes na base? (S - Sim / N - Nao): ";              
      cin>>opcao;
      if((opcao == 'S') || (opcao == 's'))
      {
         grava();
      } 
}


void mapcliente::carregardabase()
{
     char opcao;
     cout<<endl;
     cout<<"  #-#-# -> Deseja carregar informacoes da base? (S - Sim / N - Nao): ";              
     cin>>opcao;
     if((opcao == 'S') || (opcao == 's'))
     {
       carregar();
     }    
}

bool mapcliente::existecliente(int numcliente)
{
    map<int, cliente>::iterator auxclien = mapcli.find(numcliente);
    if (auxclien == mapcli.end())
        return false;
    return true;
} 

void mapcliente::cadcliente()
{  
    map<int, cliente>::iterator aux; 
    pair<map<int,cliente>::iterator,bool> ret;
    cliente auxcliente;
    int opcao, codigo;
    char sexo;
    string nome,cpf,rg,endereco,bairro,cidade,estado,cep,telefone,estado_civil;
    
    //    "$ =============================================================== $\n"    
    cout<<"  principal -> clientes -> CADASTRAR                               \n";
    cout<<endl;
    cout<<"  #-#-#-#-#-#-#-#      CADASTRO DE CLIENTES       #-#-#-#-#-#-#-#  \n";
    cout<<"  ===============================================================  \n"; 
    if(mapcli.empty())
    {
       carregardabase();     
    }
    cout<<"  #-#-# -> Codigo: ";
    cin>>codigo;
    cout<<endl;        

    if(!existecliente(codigo))
    {
        cout<<"  #-#-# -> Nome: ";
        fflush(stdin);
        getline(cin, nome);
        cout<<endl;
        cout<<"  #-#-# -> Cpf: ";
        fflush(stdin);
        getline(cin, cpf);
        cout<<endl;
        cout<<"  #-#-# -> Rg: ";
        fflush(stdin);
        getline(cin, rg);
        cout<<endl;
        cout<<"  #-#-# -> Sexo (M / F): ";
        cin>>sexo;
        cout<<endl;
        cout<<"  #-#-# -> Endereco: ";
        fflush(stdin);
        getline(cin, endereco);
        cout<<endl;
        cout<<"  #-#-# -> Bairro: ";
        fflush(stdin);
        getline(cin, bairro);
        cout<<endl;
        cout<<"  #-#-# -> Cidade: ";
        fflush(stdin);
        getline(cin, cidade);	
        cout<<endl;
        cout<<"  #-#-# -> Estado: ";	   			        
        fflush(stdin);
        getline(cin, estado);		  
        cout<<endl;
        cout<<"  #-#-# -> Cep: "; 			        
        fflush(stdin);
        getline(cin, cep);			   		
        cout<<endl;
        cout<<"  #-#-# -> Telefone: ";	        
        fflush(stdin);
        getline(cin, telefone);    
        cout<<endl;
        cout<<"  #-#-# -> Estado Civil: ";
        fflush(stdin);
        getline(cin, estado_civil);
        cout<<endl;
        
        auxcliente.setnome(nome);
        auxcliente.setcpf(cpf);
        auxcliente.setrg(rg);
        auxcliente.setsexo(sexo);
        auxcliente.setendereco(endereco);
        auxcliente.setbairro(bairro);
        auxcliente.setcidade(cidade);
        auxcliente.setestado(estado);
        auxcliente.setcep(cep);
        auxcliente.settelefone(telefone);
        auxcliente.setestado_civil(estado_civil);
        
        ret = mapcli.insert(make_pair(codigo, auxcliente)); 
        if(ret.second ==  true)
        {
            cout<<"  #-#-#-#-#-#-#-#  CLIENTE CADASTRADO COM SUCESSO #-#-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";    
            salvanabase();                                
        }
        else
        {
            cout<<"  #-#-#-#-#-#-#-#  OCORREU ERRO NO CADASTRAMENTO  #-#-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n"; 
        }         
    }    
    else
    {
        cout<<"  #-#-#-#-#-#-#-#         CODIGO EXISTENTE        #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";
    }   		
}	  

void mapcliente::gerarelatoriogeral() // gravar o arquivo
{
     map<int, cliente>::iterator aux = mapcli.begin();
     fstream arq; 
     string sexo; 
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> clientes -> relatorios -> GERAL                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CLIENTES          #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";
     
   if(!mapcli.empty())
   {
       arq.clear();
       arq.open("relatorio_clientes.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"####################### - RELATORIO DE CLIENTES - ########################### \n";
         arq<<"\n\n";
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {
               if(aux->second.getsexo() == 'F')
               {
                 sexo = "Feminino";
               }
               else
               {
                 sexo = "Masculino";
               }      
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<aux->second.getnome()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => CPF: "<<aux->second.getcpf()<<" \t=> RG: "<<aux->second.getrg()<<" \t=> SEXO: "<<sexo<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" \t => ESTADO CIVIL: "<<aux->second.getestado_civil()<<"\n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";                        
           
         }
         cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_clientes.txt"<<endl;
         cout<<"  Concluido...\n";         
         system("pause");
         cout<<endl;
         cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
         arq.close();
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO     #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
} 

void mapcliente::relatorionome() // gravar o arquivo
{
     map<int, cliente>::iterator aux = mapcli.begin();
     fstream arq;  
     string nome,sexo;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> clientes -> relatorios -> NOME                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CLIENTES          #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcli.empty())
   {
       cout<<"  #-#-# -> Nome: ";
       fflush(stdin);
       getline(cin, nome);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM NOME "<<nome<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_nome_clientes.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE CLIENTES POR NOME   - #################### \n";
         arq<<"\n\n";  
                  
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {  
           if(aux->second.getnome() == nome)
           {
               n++;
               if(aux->second.getsexo() == 'F')
               {
                 sexo = "Feminino";
               }
               else
               {
                 sexo = "Masculino";
               }
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<aux->second.getnome()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => CPF: "<<aux->second.getcpf()<<" \t=> RG: "<<aux->second.getrg()<<" \t=> SEXO: "<<sexo<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" \t => ESTADO CIVIL: "<<aux->second.getestado_civil()<<"\n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";                                           
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO ENCONTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_nome_clientes.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO     #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::relatorioendereco() // gravar o arquivo
{
     map<int, cliente>::iterator aux = mapcli.begin();
     fstream arq;  
     string endereco,sexo;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> clientes -> relatorios -> ENDERECO                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CLIENTES          #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcli.empty())
   {
       cout<<"  #-#-# -> Endereco: ";
       fflush(stdin);
       getline(cin, endereco);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM ENDERECO "<<endereco<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_endereco_clientes.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE CLIENTES POR ENDERECO - ################## \n";
         arq<<"\n\n";  
                  
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {  
           if(aux->second.getendereco() == endereco)
           {
               n++;
               if(aux->second.getsexo() == 'F')
               {
                 sexo = "Feminino";
               }
               else
               {
                 sexo = "Masculino";
               }
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<aux->second.getnome()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => CPF: "<<aux->second.getcpf()<<" \t=> RG: "<<aux->second.getrg()<<" \t=> SEXO: "<<sexo<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" \t => ESTADO CIVIL: "<<aux->second.getestado_civil()<<"\n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";                                           
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO ENCONTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_endereco_clientes.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO     #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::relatoriobairro() // gravar o arquivo
{
     map<int, cliente>::iterator aux = mapcli.begin();
     fstream arq;  
     string bairro,sexo;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> clientes -> relatorios -> BAIRRO                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CLIENTES          #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcli.empty())
   {
       cout<<"  #-#-# -> Bairro: ";
       fflush(stdin);
       getline(cin, bairro);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM BAIRRO "<<bairro<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_bairro_clientes.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE CLIENTES POR BAIRRO - #################### \n";
         arq<<"\n\n";  
                  
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {  
           if(aux->second.getbairro() == bairro)
           {
               n++;
               if(aux->second.getsexo() == 'F')
               {
                 sexo = "Feminino";
               }
               else
               {
                 sexo = "Masculino";
               }
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<aux->second.getnome()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => CPF: "<<aux->second.getcpf()<<" \t=> RG: "<<aux->second.getrg()<<" \t=> SEXO: "<<sexo<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" \t => ESTADO CIVIL: "<<aux->second.getestado_civil()<<"\n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";                                           
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO ENCONTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_bairro_clientes.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO     #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::relatoriocidade() // gravar o arquivo
{
     map<int, cliente>::iterator aux = mapcli.begin();
     fstream arq;  
     string cidade,sexo;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> clientes -> relatorios -> CIDADE                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CLIENTES          #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcli.empty())
   {
       cout<<"  #-#-# -> Cidade: ";
       fflush(stdin);
       getline(cin, cidade);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CIDADE "<<cidade<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_cidade_clientes.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE CLIENTES POR CIDADE - #################### \n";
         arq<<"\n\n";  
                  
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {  
           if(aux->second.getcidade() == cidade)
           {
               n++;
               if(aux->second.getsexo() == 'F')
               {
                 sexo = "Feminino";
               }
               else
               {
                 sexo = "Masculino";
               }
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<aux->second.getnome()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => CPF: "<<aux->second.getcpf()<<" \t=> RG: "<<aux->second.getrg()<<" \t=> SEXO: "<<sexo<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" \t => ESTADO CIVIL: "<<aux->second.getestado_civil()<<"\n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";                                           
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO ENCONTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_cidade_clientes.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO     #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::relatorioestado() // gravar o arquivo
{
     map<int, cliente>::iterator aux = mapcli.begin();
     fstream arq;  
     string estado,sexo;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> clientes -> relatorios -> ESTADO                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CLIENTES          #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcli.empty())
   {
       cout<<"  #-#-# -> Estado: ";
       fflush(stdin);
       getline(cin, estado);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM ESTADO "<<estado<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_estado_clientes.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE CLIENTES POR ESTADO - #################### \n";
         arq<<"\n\n";  
                  
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {  
           if(aux->second.getestado() == estado)
           {
               n++;
               if(aux->second.getsexo() == 'F')
               {
                 sexo = "Feminino";
               }
               else
               {
                 sexo = "Masculino";
               }
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<aux->second.getnome()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => CPF: "<<aux->second.getcpf()<<" \t=> RG: "<<aux->second.getrg()<<" \t=> SEXO: "<<sexo<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" \t => ESTADO CIVIL: "<<aux->second.getestado_civil()<<"\n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";                                           
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO ENCONTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_estado_clientes.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO     #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::relatoriocep() // gravar o arquivo
{
     map<int, cliente>::iterator aux = mapcli.begin();
     fstream arq;  
     string cep,sexo;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> clientes -> relatorios -> CEP                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CLIENTES          #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcli.empty())
   {
       cout<<"  #-#-# -> Cep: ";
       fflush(stdin);
       getline(cin, cep);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CEP "<<cep<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_cep_clientes.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE CLIENTES POR CEP - #################### \n";
         arq<<"\n\n";  
                  
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {  
           if(aux->second.getcep() == cep)
           {
               n++;
               if(aux->second.getsexo() == 'F')
               {
                 sexo = "Feminino";
               }
               else
               {
                 sexo = "Masculino";
               }
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<aux->second.getnome()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => CPF: "<<aux->second.getcpf()<<" \t=> RG: "<<aux->second.getrg()<<" \t=> SEXO: "<<sexo<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" \t => ESTADO CIVIL: "<<aux->second.getestado_civil()<<"\n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";                                           
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO ENCONTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_cep_clientes.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO     #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::relatorioestadocivil() // gravar o arquivo
{
     map<int, cliente>::iterator aux = mapcli.begin();
     fstream arq;  
     string estado_civil,sexo;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> clientes -> relatorios -> ESTADO CIVIL                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CLIENTES          #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcli.empty())
   {
       cout<<"  #-#-# -> Estado Civil: ";
       fflush(stdin);
       getline(cin, estado_civil);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM ESTADO CIVIL "<<estado_civil<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_estadocivil_clientes.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE CLIENTES POR ESTADO CIVIL - ############## \n";
         arq<<"\n\n";  
                  
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {  
           if(aux->second.getestado_civil() == estado_civil)
           {
               n++;
               if(aux->second.getsexo() == 'F')
               {
                 sexo = "Feminino";
               }
               else
               {
                 sexo = "Masculino";
               }
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<aux->second.getnome()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => CPF: "<<aux->second.getcpf()<<" \t=> RG: "<<aux->second.getrg()<<" \t=> SEXO: "<<sexo<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" \t => ESTADO CIVIL: "<<aux->second.getestado_civil()<<"\n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";                                           
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO ENCONTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_estadocivil_clientes.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO     #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::relatoriosexo() // gravar o arquivo
{
     map<int, cliente>::iterator aux = mapcli.begin();
     fstream arq;  
     string nome,sexo1;
     char sexo;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> clientes -> relatorios -> SEXO                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CLIENTES          #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcli.empty())
   {
       cout<<"  #-#-# -> Sexo (M / F): ";
       cin>>sexo;

       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM SEXO "<<sexo<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_sexo_clientes.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"################### - RELATORIO DE CLIENTES POR SEXO   - #################### \n";
         arq<<"\n\n";  
                  
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {  
           if(aux->second.getsexo() == sexo)
           {
               n++;
               if(aux->second.getsexo() == 'F')
               {
                 sexo1 = "Feminino";
               }
               else
               {
                 sexo1 = "Masculino";
               }
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<aux->second.getnome()<<" \t=> TELEFONE: "<<aux->second.gettelefone()<<"\n";
               arq<<" => CPF: "<<aux->second.getcpf()<<" \t=> RG: "<<aux->second.getrg()<<" \t=> SEXO: "<<sexo1<<"\n";
               arq<<" => ENDERECO: "<<aux->second.getendereco()<<" \t => ESTADO CIVIL: "<<aux->second.getestado_civil()<<"\n";
               arq<<" => CEP: "<<aux->second.getcep()<<" \t=> BAIRRO: "<<aux->second.getbairro()<<"\n";
               arq<<" => CIDADE: "<<aux->second.getcidade()<<" \t=> ESTADO: "<<aux->second.getestado()<<"\n\n";                                           
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO ENCONTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_sexo_clientes.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO     #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::removecliente()
{      
    //map<int, conta>::iterator aux;
    int cod;
    //    "$ =============================================================== $\n"    
    cout<<"  principal -> clientes -> REMOVER                               \n";
    cout<<endl;
    cout<<"  #-#-#-#-#-#-#-#       REMOCAO DE CLIENTES       #-#-#-#-#-#-#-#  \n";
    cout<<"  ===============================================================  \n";  
    cout<<endl;
    if(mapcli.empty())
    {
       carregardabase();
    }
    
    if (!mapcli.empty())
    {
         cout<<"  #-#-# -> Codigo: ";
         cin>>cod;
         cout<<endl;                     
         
         if(existecliente(cod))
         {
              cout<<"  AGUARDE UM MOMENTO....VERIFICANDO CLIENTE COM CODIGO "<<cod<<endl;
              system("pause");
              cout<<endl;
              //for(aux = cont.begin(); aux != cont.end(); aux++)
              //{
                //if(aux->second.getclienteconta() == cod)
                //{
                  //  cout<<"  #-#-#-#-#-#-#-#  CLIENTE NAO PODE SER REMOVIDO  #-#-#-#-#-#-#-#  \n";
                   // cout<<"  #-#-#-#-#  O MESMO ESTA VINCULADO A UMA CONTA BANCARIA  #-#-#-#  \n";
                    //cout<<"  ===============================================================  \n";
                    //cout<<endl;                  
               // }
               // else
               // {
                    cout<<"  AGUARDE UM MOMENTO....REMOVENDO CLIENTE COM CODIGO "<<cod<<endl;
                    system("pause");
                    cout<<endl;
                    mapcli.erase(cod);
                    cout<<"  #-#-#-#-#-#-#-#  CLIENTE REMOVIDO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
                    cout<<"  ===============================================================  \n";
                    salvanabase();
                //}
             // }                            
         }
         else
         {
              cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
              cout<<"  ===============================================================  \n"; 
         }
    }
    else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editanome()
{
     string nome;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> NOME                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Nome: ";
       fflush(stdin);
       getline(cin, nome);
       mapcli[cod].setnome(nome);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n"; 
       salvanabase();             
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editacpf()
{
     string cpf;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> CPF                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Cpf: ";
       fflush(stdin);
       getline(cin, cpf);
       mapcli[cod].setcpf(cpf);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n"; 
       salvanabase();             
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editarg()
{
     string rg;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> RG                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Rg: ";
       fflush(stdin);
       getline(cin, rg);
       mapcli[cod].setrg(rg);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";
       salvanabase();              
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editasexo()
{
    char sexo;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> SEXO                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Sexo: ";      
       cin>>sexo;
       mapcli[cod].setsexo(sexo);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";     
       salvanabase();         
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editaendereco()
{
  string endereco;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> ENDERECO                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Endereco: ";
       fflush(stdin);
       getline(cin, endereco);
       mapcli[cod].setendereco(endereco);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";  
       salvanabase();            
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editabairro()
{
  string bairro;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> BAIRRO                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Bairro: ";
       fflush(stdin);
       getline(cin, bairro);
       mapcli[cod].setbairro(bairro);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";  
       salvanabase();            
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editacidade()
{
  string cidade;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> CIDADE                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Cidade: ";
       fflush(stdin);
       getline(cin, cidade);
       mapcli[cod].setcidade(cidade);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";   
       salvanabase();           
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editaestado()
{
  string estado;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> ESTADO                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Estado: ";
       fflush(stdin);
       getline(cin, estado);
       mapcli[cod].setestado(estado);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n"; 
       salvanabase();             
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editacep()
{
  string cep;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> CEP                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Cep: ";
       fflush(stdin);
       getline(cin, cep);
       mapcli[cod].setcep(cep);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";
       salvanabase();              
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editatelefone()
{
  string telefone;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> TELEFONE                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Telefone: ";
       fflush(stdin);
       getline(cin, telefone);
       mapcli[cod].settelefone(telefone);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n"; 
       salvanabase();             
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}


void mapcliente::editaestado_civil()
{
  string estado_civil;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> clientes -> editar -> ESTADO CIVIL                        \n";
   cout<<endl;
   if(mapcli.empty())
   {
     carregardabase();
   }   
   
   if (!mapcli.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Estado Civil: ";
       fflush(stdin);
       getline(cin, estado_civil);
       mapcli[cod].setestado_civil(estado_civil);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";   
       salvanabase();           
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapcliente::editatodas()
{      
    char sexo;
    string nome,cpf,rg,endereco,bairro,cidade,estado,cep,telefone,estado_civil;
    int cod;
    cliente auxc; 
    //    "$ =============================================================== $\n"    
    cout<<"  principal -> clientes -> editar -> TODOS OS CAMPOS               \n";
    cout<<endl;
    if(mapcli.empty())
   {
     carregardabase();
   }
   
    if (!mapcli.empty())
    {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existecliente(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Nome: ";
       fflush(stdin);
       getline(cin, nome);
       cout<<"  #-#-# -> Cpf: ";
       fflush(stdin);
       getline(cin, cpf);
       cout<<"  #-#-# -> rg: ";
       fflush(stdin);
       getline(cin, rg);
       cout<<"  #-#-# -> Sexo: ";
       cin>>sexo;
       cout<<"  #-#-# -> Endereco: ";
       fflush(stdin);
       getline(cin, endereco);       
       cout<<"  #-#-# -> Bairro: ";
       fflush(stdin);
       getline(cin, bairro);
       cout<<"  #-#-# -> Cidade: ";
       fflush(stdin);
       getline(cin, cidade);
       cout<<"  #-#-# -> Estado: ";
       fflush(stdin);
       getline(cin, estado);
       cout<<"  #-#-# -> Cep: ";
       fflush(stdin);
       getline(cin, cep);
       cout<<"  #-#-# -> Telefone: ";
       fflush(stdin);
       getline(cin, telefone);
       cout<<"  #-#-# -> Estado Civil: ";
       fflush(stdin);
       getline(cin, estado_civil);
       
       auxc.setnome(nome);
       auxc.setcpf(cpf);
       auxc.setrg(rg);
       auxc.setsexo(sexo);
       auxc.setendereco(endereco);
       auxc.setcidade(cidade);
       auxc.setbairro(bairro);
       auxc.setestado(estado);
       auxc.setcep(cep);
       auxc.settelefone(telefone);
       auxc.setestado_civil(estado_civil);
       mapcli[cod] = auxc;
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#  CAMPOS EDITADOS COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";     
       salvanabase();         
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
   
}

void mapcliente::mostratodas()
{
     map<int, cliente>::iterator aux;
     if(mapcli.empty())
     {
       carregardabase();
     }
     if(!mapcli.empty())
     {
         //    "$ =============================================================== $\n"    
         cout<<"  principal -> clientes -> consultar -> TODOS                       \n";
         cout<<endl;
         for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
         {
             aux->second.mostracliente();
         }    
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO     #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     }         
}   

void mapcliente::mostracodigo()
{
     map<int, cliente>::iterator aux;
     int cod;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> CODIGO                       \n";
        cout<<endl;
     if(mapcli.empty())
      {
       carregardabase();
     }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Codigo: ";
        cin>>cod;
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<cod<<endl;
        system("pause");
        cout<<endl;
        
        aux = mapcli.find(cod);
        
        if(aux != mapcli.end())
        {
          cout<<"  #-#-# -> Codigo localizado."<<endl;
          aux->second.mostracliente();         
          cout<<endl;
                      
         }
         else
         {
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostranome()
{
     map<int, cliente>::iterator aux;
     string nome;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> NOME                       \n";
        cout<<endl;
     if(mapcli.empty())
     {
      carregardabase();
     }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Nome: ";
        fflush(stdin);
        getline(cin, nome);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM NOME "<<nome<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.getnome() == nome)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostracpf()
{
     map<int, cliente>::iterator aux;
     string cpf;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> CPF                       \n";
        cout<<endl;
     if(mapcli.empty())
   {
     carregardabase();
   }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Cpf: ";
        fflush(stdin);
        getline(cin, cpf);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CPF "<<cpf<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.getcpf() == cpf)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostrarg()
{
     map<int, cliente>::iterator aux;
     string rg;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> RG                       \n";
        cout<<endl;
     if(mapcli.empty())
   {
     carregardabase();
   }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Rg: ";
        fflush(stdin);
        getline(cin, rg);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM RG "<<rg<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.getrg() == rg)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostrasexo()
{
     map<int, cliente>::iterator aux;
     char sexo;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> SEXO                       \n";
        cout<<endl;
    if(mapcli.empty())
    {
       carregardabase();
    }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Sexo: ";
        cin>>sexo;
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM SEXO "<<sexo<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.getsexo() == sexo)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostraendereco()
{
     map<int, cliente>::iterator aux;
     string endereco;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> ENDERECO                       \n";
        cout<<endl;
     if(mapcli.empty())
    {
       carregardabase();
    }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Endereco: ";
        fflush(stdin);
        getline(cin, endereco);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM ENDERECO "<<endereco<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.getendereco() == endereco)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostracidade()
{
     map<int, cliente>::iterator aux;
     string cidade;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> CIDADE                       \n";
        cout<<endl;
        if(mapcli.empty())
    {
       carregardabase();
    }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Cidade: ";
        fflush(stdin);
        getline(cin, cidade);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CIDADE "<<cidade<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.getcidade() == cidade)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostrabairro()
{
     map<int, cliente>::iterator aux;
     string bairro;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> BAIRRO                       \n";
        cout<<endl;
        if(mapcli.empty())
    {
       carregardabase();
    }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Bairro: ";
        fflush(stdin);
        getline(cin, bairro);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM BAIRRO "<<bairro<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.getbairro() == bairro)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostraestado()
{
     map<int, cliente>::iterator aux;
     string estado;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> ESTADO                       \n";
        cout<<endl;
        if(mapcli.empty())
    {
       carregardabase();
    }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Estado: ";
        fflush(stdin);
        getline(cin, estado);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM ESTADO "<<estado<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.getestado() == estado)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostracep()
{
     map<int, cliente>::iterator aux;
     string cep;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> CEP                       \n";
        cout<<endl;
        if(mapcli.empty())
    {
       carregardabase();
    }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Cep: ";
        fflush(stdin);
        getline(cin, cep);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CEP "<<cep<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.getcep() == cep)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostratelefone()
{
     map<int, cliente>::iterator aux;
     string telefone;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> TELEFONE                       \n";
        cout<<endl;
        if(mapcli.empty())
    {
       carregardabase();
    }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Telefone: ";
        fflush(stdin);
        getline(cin, telefone);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM TELEFONE "<<telefone<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.gettelefone() == telefone)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapcliente::mostraestado_civil()
{
     map<int, cliente>::iterator aux;
     string estado_civil;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> clientes -> consultar -> ESTADO CIVIL              \n";
        cout<<endl;
        if(mapcli.empty())
    {
       carregardabase();
    }
     if(!mapcli.empty())
     {         
        cout<<"  #-#-# -> Estado Civil: ";
        fflush(stdin);
        getline(cin, estado_civil);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM ESTADO CIVIL "<<estado_civil<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcli.begin(); aux != mapcli.end(); aux++)
        {
          if(aux->second.getestado_civil() == estado_civil)
          {
            n++;            
            aux->second.mostracliente();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CLIENTE NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUM CLIENTE CADASTRADO    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

string mapcliente::nomecliente(int numcliente)
{
    map<int, cliente>::iterator auxc = mapcli.find(numcliente);
    if (auxc != mapcli.end())
        return auxc->second.getnome();
    //return true;
} 
 
/*
  Fun��es da classe mapconta
*/

int mapconta::menuconta()
{
   int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> CONTAS                                            \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#  GERENCIAMENTO DE CONTAS        #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (1) - CARREGAR BASE          #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - GRAVAR BASE            #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - CADASTRAR              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - REMOVER                #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (5) - EDITAR                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (6) - CONSULTAR              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (7) - RELATORIOS             #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;
}

int mapconta::menucontaconsulta()
{
   int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> contas -> CONSULTAR                                 \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#      CONSULTAR CONTAS           #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (1) - CODIGO                 #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - CLIENTE                #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - AGENCIA                #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - TIPO                   #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (5) - DATA CADASTRO          #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (6) - ATENDENTE              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (7) - TODAS                  #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;
}

int mapconta::menucontarel()
{
   int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> contas ->  RELATORIOS                            \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#     RELATORIOS CONTAS           #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (1) - GERAL                  #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (2) - TIPO                   #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - DATA CADASTRO          #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (4) - ATENDENTE              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;
}

int mapconta::menueditaconta()
{
   int opcao;  
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> contas -> EDITAR                                    \n";
   cout<<endl;
   cout<<"  #-#-#-#-#-#-#-#     EDITAR CONTAS               #-#-#-#-#-#-#-#  \n";   
   cout<<"  #-#-#-#-#-#-#-#    (1) - TIPO                   #-#-#-#-#-#-#-#  \n"; 
   cout<<"  #-#-#-#-#-#-#-#    (2) - DATA CADASTRO          #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (3) - ATENDENTE              #-#-#-#-#-#-#-#  \n";
   cout<<"  #-#-#-#-#-#-#-#    (0) - MENU ANTERIOR          #-#-#-#-#-#-#-#  \n";
   cout<<"  Informe a opcao desejada: ";
   cin>>opcao;
   
   return opcao;    
}

void mapconta::gerarelatoriogeral(mapcliente clien) // gravar o arquivo
{
     map<int, conta>::iterator aux = mapcont.begin();
     fstream arq; 
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> contas -> relatorios -> GERAL                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CONTAS            #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";
     
   if(!mapcont.empty())
   {
       arq.clear();
       arq.open("relatorio_contas.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"####################### - RELATORIO DE CONTAS - ########################### \n";
         arq<<"\n\n";
         for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
         {
               int cod;
               string nome;
               cod = aux->second.getclienteconta();       
               nome = clien.nomecliente(cod);               
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<nome<<" \n";
               arq<<" => AGENCIA: "<<aux->second.getagenciaconta()<<" \n";
               arq<<" => TIPO CONTA: "<<aux->second.gettipo()<<" \t => DATA CADASTRO: "<<aux->second.getdata_cadastro()<<"\n";
               arq<<" => ATENDENTE: "<<aux->second.getatendente()<<" \n";
           
         }
         cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_contas.txt"<<endl;
         cout<<"  Concluido...\n";         
         system("pause");
         cout<<endl;
         cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
         arq.close();
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
} 

void mapconta::relatoriotipo(mapcliente clien) // gravar o arquivo
{
     map<int, conta>::iterator aux = mapcont.begin();
     fstream arq;  
     string tipo;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> contas -> relatorios -> TIPO                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CONTAS            #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcont.empty())
   {
       cout<<"  #-#-# -> Tipo: ";
       fflush(stdin);
       getline(cin, tipo);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM TIPO "<<tipo<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_tipo_contas.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"######################## - RELATORIO DE CONTAS POR TIPO - ################### \n";
         arq<<"\n\n";  
                  
         for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
         {  
           if(aux->second.gettipo() == tipo)
           {
               n++;
               int cod;
               string nome;
               cod = aux->second.getclienteconta();       
               nome = clien.nomecliente(cod);               
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<nome<<" \n";
               arq<<" => AGENCIA: "<<aux->second.getagenciaconta()<<" \n";
               arq<<" => TIPO CONTA: "<<aux->second.gettipo()<<" \t => DATA CADASTRO: "<<aux->second.getdata_cadastro()<<"\n";
               arq<<" => ATENDENTE: "<<aux->second.getatendente()<<" \n";
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CONTA NAO ENCONTRADA        #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_tipo_contas.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapconta::relatoriodatacadastro(mapcliente clien) // gravar o arquivo
{
     map<int, conta>::iterator aux = mapcont.begin();
     fstream arq;  
     string data_cadastro;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> contas -> relatorios -> DATA CADASTRO                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CONTAS            #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcont.empty())
   {
       cout<<"  #-#-# -> Data cadastro: ";
       fflush(stdin);
       getline(cin, data_cadastro);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM DATA CADASTRO "<<data_cadastro<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_cadastro_contas.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"######################## - RELATORIO DE CONTAS POR DATA CADASTRO - ################### \n";
         arq<<"\n\n";  
                  
         for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
         {  
           if(aux->second.getdata_cadastro() == data_cadastro)
           {
               n++;
               int cod;
               string nome;
               cod = aux->second.getclienteconta();       
               nome = clien.nomecliente(cod);               
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<nome<<" \n";
               arq<<" => AGENCIA: "<<aux->second.getagenciaconta()<<" \n";
               arq<<" => TIPO CONTA: "<<aux->second.gettipo()<<" \t => DATA CADASTRO: "<<aux->second.getdata_cadastro()<<"\n";
               arq<<" => ATENDENTE: "<<aux->second.getatendente()<<" \n";
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CONTA NAO ENCONTRADA        #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_cadastro_contas.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapconta::relatorioatendente(mapcliente clien) // gravar o arquivo
{
     map<int, conta>::iterator aux = mapcont.begin();
     fstream arq;  
     string atendente;
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> contas -> relatorios -> ATENDENTE                    \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  RELATORIO DE CONTAS            #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcont.empty())
   {
       cout<<"  #-#-# -> Atendente: ";
       fflush(stdin);
       getline(cin, atendente);
       cout<<endl; 
       cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM ATENDENTE "<<atendente<<endl;
       system("pause");
       cout<<endl;
       int n = 0;       
         
       arq.clear();
       arq.open("relatorio_atendente_contas.txt", ios :: out);           
       if(!arq.fail())
       {
         arq<<"============================================================================= \n";
         arq<<"######################## - RELATORIO DE CONTAS POR ATENDENTE - ############## \n";
         arq<<"\n\n";  
                  
         for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
         {  
           if(aux->second.getatendente() == atendente)
           {
               n++;
               int cod;
               string nome;
               cod = aux->second.getclienteconta();       
               nome = clien.nomecliente(cod);               
               arq<<"============================================================================= \n\n";
               arq<<" => CODIGO: "<<aux->first<<" \t=> CLIENTE: "<<nome<<" \n";
               arq<<" => AGENCIA: "<<aux->second.getagenciaconta()<<" \n";
               arq<<" => TIPO CONTA: "<<aux->second.gettipo()<<" \t => DATA CADASTRO: "<<aux->second.getdata_cadastro()<<"\n";
               arq<<" => ATENDENTE: "<<aux->second.getatendente()<<" \n";
           }                  
         }
         if(n == 0)
         {       
           cout<<"  #-#-#-#-#-#-#-#     CONTA NAO ENCONTRADA        #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         else
         {         
           cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: relatorio_atendente_contas.txt"<<endl;
           cout<<"  Concluido...\n";         
           system("pause");
           cout<<endl;
           cout<<"  #-#-#-#-#-#-#-#  RELATORIO GERADO COM SUCESSO   #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n";
           arq.close();
         }    
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapconta::carregar()
{     
     int codigo, cliente, agencia;
     fstream arq;
     string tipo, data_cadastro,atendente;
     conta auxconta;    
     arq.clear();
     mapcont.clear();
     arq.open("base_contas.txt", ios::in);
     if(!arq.fail())     
     {       
       while(!arq.eof())
       {
         //pega dados do arquivo               
         arq>>codigo>>cliente>>agencia>>tipo>>data_cadastro>>atendente;
         auxconta.setclienteconta(cliente);         
         auxconta.setagenciaconta(agencia);
         auxconta.settipo(tipo);
         auxconta.setdata_cadastro(data_cadastro);
         auxconta.setatendente(atendente);        
         mapcont.insert(make_pair(codigo, auxconta));
                  
       }
            cout<<"  #-#-#-#-#-#-#-#  CONTAS IMPORTADAS COM SUCESSO  #-#-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
     arq.close();
     }
     else
     {
         cout<<"Arquivo nao pode ser aberto";
     }
}

void mapconta::grava() // gravar o arquivo
{
     map<int, conta>::iterator aux = mapcont.begin();
     fstream arq;  
     //    "$ =============================================================== $\n"    
     cout<<"  principal -> contas -> GRAVAR BASE                               \n";
     cout<<endl;
     cout<<"  #-#-#-#-#-#-#-#  GRAVACAO DE CONTAS             #-#-#-#-#-#-#-#  \n";
     cout<<"  ===============================================================  \n";

   if(!mapcont.empty())
   {
       arq.clear();
       arq.open("base_contas.txt", ios :: out);           
       if(!arq.fail())
       {
         for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
         {               
           arq<< aux-> first <<"\t"<< aux -> second.getclienteconta()<<"\t"<< aux->second.getagenciaconta()<<"\t"<<aux->second.gettipo()<<"\t"<<aux->second.getdata_cadastro()<<"\t"<<aux->second.getatendente();
           arq<<"\n";
         }
         cout<<"  AGUARDE UM MOMENTO....GRAVANDO DADOS EM: base_contas.txt"<<endl;
         cout<<"  Concluido...\n";         
         system("pause");
         cout<<endl;
         cout<<"  #-#-#-#-#-#-#-#  GRAVACAO REALIZADA COM SUCESSO #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
         arq.close();
        }  
        else
        {
            cout<<"  #-#-#-#-#-#-#       IMPOSSIVEL ABRIR ARQUIVO      #-#-#-#-#-#-#  \n";
            cout<<"  ===============================================================  \n";
        }         
    }
        else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA    #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}

void mapconta::salvanabase()
{     
      char opcao;
      cout<<endl;
      cout<<"  #-#-# -> Deseja salvar as alteracoes na base? (S - Sim / N - Nao): ";              
      cin>>opcao;
      if((opcao == 'S') || (opcao == 's'))
      {
         grava();
      } 
}

void mapconta::carregardabase()
{
     char opcao;
     cout<<endl;
     cout<<"  #-#-# -> Deseja carregar informacoes da base? (S - Sim / N - Nao): ";              
     cin>>opcao;
     if((opcao == 'S') || (opcao == 's'))
     {
       carregar();
     }    
}

bool mapconta::existeconta(int numconta)
{
    map<int, conta>::iterator auxcont = mapcont.find(numconta);
    if (auxcont == mapcont.end())
        return false;
    return true;
} 

void mapconta::mostratodas(mapcliente clien)
{
     map<int, conta>::iterator aux;
     map<int, cliente>::iterator auxc;
     if(mapcont.empty())
     {
       carregardabase();
     }
     if(!mapcont.empty())
     {
         //    "$ =============================================================== $\n"    
         cout<<"  principal -> contas -> consultar -> TODOS                       \n";
         cout<<endl;
         
         for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
         {              
             int cod;
             string nome;
             cod = aux->second.getclienteconta();
             //cout<<cod;             
             nome = clien.nomecliente(cod);
             cout<<"  #-#-# -> Cliente: "<<nome;              
             cout<<endl;    
             aux->second.mostraconta();
         }    
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA      #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     }         
}  

void mapconta::aberturaconta(mapcliente clien, mapagencia agen)
{
	map<int, conta>::iterator auxcont; 
	map<int, cliente>::iterator auxcli;
	map<int, agencia>::iterator auxag;	
	pair<map<int,conta>::iterator,bool> ret;
    conta auxconta;
    int opcao,codigo,codcliente, codagencia; 
	string tipo, data_cadastro,atendente;  
	
	//    "$ =============================================================== $\n"    
    cout<<"  principal -> contas -> ABERTURA CONTA                            \n";
    cout<<endl;
    cout<<"  #-#-#-#-#-#-#-#  ABERTURA DE CONTAS             #-#-#-#-#-#-#-#  \n";
    cout<<"  ===============================================================  \n";
    
    if(agen.vazio())
    {
       cout<<"  E' preciso carregar ou cadastrar agencia, para abrir contas."<<endl;
    }
    else
    {    
         if(clien.vazio())
         {
            cout<<" E�preciso carregar ou cadastrar cliente, para abrir contas."<<endl;
         }
         else
         {
         
            if(mapcont.empty())
             {   
               carregardabase();
             } 
            cout<<"  #-#-# -> Codigo: ";
            cin>>codigo;
            cout<<endl;
            cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM CODIGO "<<codigo<<endl;
            system("pause");
            cout<<endl;
                
            if(!existeconta(codigo))
            {
                cout<<"  #-#-# -> Cliente: ";
                cin>>codcliente;
                cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CLIENTE COM CODIGO "<<codcliente<<endl;
                system("pause");
                cout<<endl;                  
                
                if(clien.existecliente(codcliente))
                {
                    cout<<"  #-#-# -> Cliente localizado."<<endl;
                    cout<<"  #-#-# -> Agencia: ";
                    cin>>codagencia;       
                    cout<<"  AGUARDE UM MOMENTO....PESQUISANDO AGENCIA COM CODIGO "<<codagencia<<endl;
                    system("pause");
                    cout<<endl;
                    
                    if(agen.existeagencia(codagencia))
                    {
                        cout<<"  #-#-# -> Tipo: ";
                        fflush(stdin);
                        getline(cin, tipo); 
                        cout<<endl;                             
                        cout<<"  #-#-# -> Data Cadastro: ";
                        fflush(stdin);
                        getline(cin, data_cadastro);
                        cout<<endl;
                        cout<<"  #-#-# -> Atendente: ";
                        fflush(stdin);
                        getline(cin, atendente);
                        cout<<endl;                
                        auxconta.setclienteconta(codcliente);                
                        auxconta.setagenciaconta(codagencia);
                        auxconta.settipo(tipo);
                        auxconta.setdata_cadastro(data_cadastro);
                        auxconta.setatendente(atendente);                       
                		
                		//Adiciona
                        ret = mapcont.insert(make_pair(codigo, auxconta));
                        if(ret.second ==  true)
                        {
                            cout<<"  #-#-#-#-#-#-#-#     CONTA ABERTA COM SUCESSO    #-#-#-#-#-#-#-#  \n";
                            cout<<"  ===============================================================  \n";
                            salvanabase();                                    
                        }
                        else
                        {
                            cout<<"  #-#-#-#-#-#-#-#  OCORREU ERRO NO CADASTRAMENTO  #-#-#-#-#-#-#-#  \n";
                            cout<<"  ===============================================================  \n"; 
                        }    
                    } 
                    else
                    {
                        cout<<"  #-#-#-#-#-#-#-#       AGENCIA INEXISTENTE       #-#-#-#-#-#-#-#  \n";
                        cout<<"  ===============================================================  \n";    
                    }       
                    
                    
                }    
                else
                {
                    cout<<"  #-#-#-#-#-#-#-#       CLIENTE INEXISTENTE       #-#-#-#-#-#-#-#  \n";
                    cout<<"  ===============================================================  \n";
                } 
                
            }   
            else
            {
                cout<<"  #-#-#-#-#-#-#-#         CODIGO EXISTENTE        #-#-#-#-#-#-#-#  \n";
                cout<<"  ===============================================================  \n";
            }
         }     
    }
    
}

void mapconta::editatipo()
{
  string tipo;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> contas -> editar -> TIPO                           \n";
   cout<<endl;
   if(mapcont.empty())
     {
       carregardabase();
     }
   
   if (!mapcont.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeconta(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Tipo: ";
       fflush(stdin);
       getline(cin, tipo);
       mapcont[cod].settipo(tipo);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n"; 
       salvanabase();             
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CONTA NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA      #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapconta::editadatacadastro()
{
  string data_cadastro;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> contas -> editar -> DATA CADASTRO                    \n";
   cout<<endl;
   if(mapcont.empty())
     {
       carregardabase();
     }
   
   if (!mapcont.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeconta(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Data Cadastro: ";
       fflush(stdin);
       getline(cin, data_cadastro);
       mapcont[cod].setdata_cadastro(data_cadastro);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";    
       salvanabase();          
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CONTA NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA      #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapconta::editaatendente()
{
  string atendente;
   int cod; 
   //    "$ =============================================================== $\n"    
   cout<<"  principal -> contas -> editar -> ATENDENTE                    \n";
   cout<<endl;
   if(mapcont.empty())
     {
       carregardabase();
     }
   
   if (!mapcont.empty())
   {
     cout<<"  #-#-# -> Codigo: ";
     cin>>cod;
     cout<<endl; 
     cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM CODIGO "<<cod<<endl;
     system("pause");
     cout<<endl;
     if(existeconta(cod))
     {
       cout<<"  #-#-# -> Codigo localizado."<<endl;
       cout<<"  #-#-# -> Atendente: ";
       fflush(stdin);
       getline(cin, atendente);
       mapcont[cod].setatendente(atendente);
       cout<<endl;
       cout<<"  #-#-#-#-#-#-#-#    CAMPO EDITADO COM SUCESSO    #-#-#-#-#-#-#-#  \n";
       cout<<"  ===============================================================  \n";      
       salvanabase();        
      }
      else
      {
        cout<<"  #-#-#-#-#-#-#-#     CONTA NAO CADASTRADO      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n"; 
      }
    }
    else
    {
      cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA      #-#-#-#-#-#-#-#  \n";
      cout<<"  ===============================================================  \n";         
    }
}

void mapconta::mostracodigo(mapcliente clien)
{
     map<int, conta>::iterator aux;
     int cod;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> contas -> consultar -> CODIGO                       \n";
        cout<<endl;
        if(mapcont.empty())
     {
       carregardabase();
     }
     if(!mapcont.empty())
     {         
        cout<<"  #-#-# -> Codigo: ";
        cin>>cod;
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM CODIGO "<<cod<<endl;
        system("pause");
        cout<<endl;
        
        aux = mapcont.find(cod);
        
        if(aux != mapcont.end())
        {
          cout<<"  #-#-# -> Codigo localizado."<<endl;          
          int cod;
          string nome;
          cod = aux->second.getclienteconta();
          //cout<<cod;             
          nome = clien.nomecliente(cod);
          cout<<"  #-#-# -> Cliente: "<<nome;              
          cout<<endl;  
          aux->second.mostraconta();         
          cout<<endl;
                      
         }
         else
         {
           cout<<"  #-#-#-#-#-#-#-#     CONTA NAO CADASTRADA        #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
         }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA      #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapconta::mostratipo(mapcliente clien)
{
     map<int, conta>::iterator aux;
     string tipo;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> contas -> consultar -> TIPO                       \n";
        cout<<endl;
        if(mapcont.empty())
     {
       carregardabase();
     }
     if(!mapcont.empty())
     {         
        cout<<"  #-#-# -> Tipo: ";
        fflush(stdin);
        getline(cin, tipo);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM TIPO "<<tipo<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
        {
          if(aux->second.gettipo() == tipo)
          {
            n++;            
            int cod;
            string nome;
            cod = aux->second.getclienteconta();
            //cout<<cod;             
            nome = clien.nomecliente(cod);
            cout<<"  #-#-# -> Cliente: "<<nome;              
            cout<<endl;
            aux->second.mostraconta();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CONTA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapconta::mostracliente(mapcliente clien)
{
     map<int, conta>::iterator aux;
     int codigo;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> contas -> consultar -> CLIENTE                       \n";
        cout<<endl;
        if(mapcont.empty())
     {
       carregardabase();
     }
     if(!mapcont.empty())
     {         
        cout<<"  #-#-# -> Codigo Cliente: ";
	cin>>codigo;	
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM CLIENTE "<<codigo<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
        {
          if(aux->second.getclienteconta() == codigo)
          {
            n++;            
            int cod;
            string nome;
            cod = aux->second.getclienteconta();
            //cout<<cod;             
            nome = clien.nomecliente(cod);
            cout<<"  #-#-# -> Cliente: "<<nome;              
            cout<<endl;
            aux->second.mostraconta();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CONTA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapconta::mostraagencia(mapcliente clien, mapagencia agen)
{
     map<int, conta>::iterator aux;
     int codigo;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> contas -> consultar -> AGENCIA                       \n";
        cout<<endl;
        if(mapcont.empty())
     {
       carregardabase();
     }
     if(!mapcont.empty())
     {         
        cout<<"  #-#-# -> Codigo Agencia: ";
	    cin>>codigo;	
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM AGENCIA "<<codigo<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
        {
          if(aux->second.getagenciaconta() == codigo)
          {
            n++;            
            int cod;
            string nome;
            cod = aux->second.getclienteconta();
            //cout<<cod;             
            nome = clien.nomecliente(cod);
            cout<<"  #-#-# -> Cliente: "<<nome;              
            cout<<endl;
            aux->second.mostraconta();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CONTA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapconta::mostradatacadastro(mapcliente clien)
{
     map<int, conta>::iterator aux;
     string data_cadastro;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> contas -> consultar -> DATA CADASTRO                      \n";
        cout<<endl;
        if(mapcont.empty())
     {
       carregardabase();
     }
     if(!mapcont.empty())
     {         
        cout<<"  #-#-# -> Data Cadastro: ";
        fflush(stdin);
        getline(cin, data_cadastro);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM DATA CADASTRO "<<data_cadastro<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
        {
          if(aux->second.getdata_cadastro() == data_cadastro)
          {
            n++;            
	    int cod;
            string nome;
            cod = aux->second.getclienteconta();
            //cout<<cod;             
            nome = clien.nomecliente(cod);
            cout<<"  #-#-# -> Cliente: "<<nome;              
            cout<<endl;
            aux->second.mostraconta();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CONTA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapconta::mostraatendente(mapcliente clien)
{
     map<int, conta>::iterator aux;
     string atendente;
      //    "$ =============================================================== $\n"    
        cout<<"  principal -> contas -> consultar -> ATENDENTE                      \n";
        cout<<endl;
        if(mapcont.empty())
     {
       carregardabase();
     }
     if(!mapcont.empty())
     {         
        cout<<"  #-#-# -> Atendente: ";
        fflush(stdin);
        getline(cin, atendente);
        cout<<endl; 
        cout<<"  AGUARDE UM MOMENTO....PESQUISANDO CONTA COM ATENDENTE "<<atendente<<endl;
        system("pause");
        cout<<endl;
        int n = 0;
        for(aux = mapcont.begin(); aux != mapcont.end(); aux++)
        {
          if(aux->second.getatendente() == atendente)
          {
            n++;            
	    int cod;
            string nome;
            cod = aux->second.getclienteconta();
            //cout<<cod;             
            nome = clien.nomecliente(cod);
            cout<<"  #-#-# -> Cliente: "<<nome;              
            cout<<endl;
            aux->second.mostraconta();
          }
        } 
        if(n == 0)
        {       
           cout<<"  #-#-#-#-#-#-#-#     CONTA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
           cout<<"  ===============================================================  \n"; 
        }
         
     }
     else
     {
         cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA    #-#-#-#-#-#-#-#  \n";
         cout<<"  ===============================================================  \n";
     } 
}

void mapconta::removeconta()
{
     int cod;    
    //    "$ =============================================================== $\n"    
    cout<<"  principal -> contas -> REMOVER                               \n";
    cout<<endl;
    cout<<"  #-#-#-#-#-#-#-#       REMOCAO DE CONTAS       #-#-#-#-#-#-#-#  \n";
    cout<<"  ===============================================================  \n";  
    cout<<endl;
    if(mapcont.empty())
     {
       carregardabase();
     }
    
    if (!mapcont.empty())
    {
         cout<<"  #-#-# -> Codigo: ";
         cin>>cod;
         cout<<endl;                     
         
         if(existeconta(cod))
         {
              cout<<"  AGUARDE UM MOMENTO....REMOVENDO CONTA COM CODIGO "<<cod<<endl;
              system("pause");
              cout<<endl;
              mapcont.erase(cod);
              cout<<"  #-#-#-#-#-#-#-#  CONTA REMOVIDA COM SUCESSO   #-#-#-#-#-#-#-#  \n";
              cout<<"  ===============================================================  \n"; 
              salvanabase();             
         }
         else
         {
              cout<<"  #-#-#-#-#-#-#-#     CONTA NAO CADASTRADA      #-#-#-#-#-#-#-#  \n";
              cout<<"  ===============================================================  \n"; 
         }
    }
    else
    {
        cout<<"  #-#-#-#-#-#-#-#   NENHUMA CONTA CADASTRADA      #-#-#-#-#-#-#-#  \n";
        cout<<"  ===============================================================  \n";         
    }
}
