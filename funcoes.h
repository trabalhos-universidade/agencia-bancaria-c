#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <map>

using namespace std;

/*
  Classe agencia
*/

class agencia
{
  private: 
        string banco;
        string endereco;
        string cidade;
        string bairro;
        string estado;
        string cep;
        string telefone;

  public:
        agencia();
        agencia(string banco, string endereco, string cidade, string bairro, string estado, string cep, string telefone);
        ~agencia();
        string getbanco();      
        void setbanco(string banco);
        string getendereco();
        void setendereco(string endereco);
        string getcidade();
        void setcidade(string cidade);
        string getbairro();
        void setbairro(string bairro);
        string getestado();
        void setestado(string estado);
        string getcep();
        void setcep(string cep);
        string gettelefone();
        void settelefone(string telefone);
        void mostraagencia();
};    

/*
  Classe cliente
*/

class cliente
{
    private:         
        string nome; 
        string cpf;
        string rg;
        char sexo;
        string endereco;
        string bairro;
        string cidade;
        string estado;
        string cep;
        string telefone;
        string estado_civil;
        
    public:
        cliente();
        cliente(string nome, string cpf, string rg, char sexo, string endereco, string bairro, string cidade, string estado, string cep, string telefone, string estado_civil);
        ~cliente();
        string getnome();
        void setnome(string nome);
        string getcpf();
        void setcpf(string cpf);
        string getrg();
        void setrg(string rg);
        char getsexo();
        void setsexo(char sexo);
        string getendereco();
        void setendereco(string endereco);
        string getbairro();
        void setbairro(string bairro);
        string getcidade();
        void setcidade(string cidade);
        string getestado();
        void setestado(string estado);
        string getcep();
        void setcep(string cep);
        string gettelefone();
        void settelefone(string telefone);
        string getestado_civil();
        void setestado_civil(string estado_civil);
        void mostracliente();  
   
        
};   

/*
  Classe conta
*/

class conta
{
      private:            
            int cliente;
            int agencia; 
            string tipo;            
            string data_cadastro;
            string atendente;
              
      public:
            conta();
            conta(int cliente, int agencia, string tipo, string data_cadastro, string atendente);
            //conta(string tipo, string num_conta, string data_cadastro, string atendente);
            ~conta();
            int getclienteconta();
            void setclienteconta(int cliente);
            int getagenciaconta();
            void setagenciaconta(int agencia);
            string gettipo();
            void settipo(string tipo);            
            string getdata_cadastro();
            void setdata_cadastro(string data_cadastro);
            string getatendente();
            void setatendente(string atendente);
            void mostraconta();      
};
