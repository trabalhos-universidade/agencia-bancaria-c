#include "funcoes.h"

/*
  Fun��es da classe agencia
*/

agencia::~agencia()
{
}

agencia::agencia()
{
}

agencia::agencia(string banco, string endereco, string cidade, string bairro, string estado, string cep, string telefone)
{
    this->banco = banco;
    this->endereco = endereco;
    this->cidade = cidade;
    this->bairro = bairro;
    this->estado = estado;
    this->cep = cep;
    this->telefone = telefone;    
}    

string agencia::getbanco()
{
    return banco;
} 

void agencia::setbanco(string banco)
{
    this->banco = banco;
}        

string agencia::getendereco()
{
    return endereco;
}    

void agencia::setendereco(string endereco)
{
    this->endereco = endereco;
}
    
string agencia::getcidade()
{
    return cidade;
}    

void agencia::setcidade(string cidade)
{
    this->cidade = cidade;
}    

string agencia::getbairro()
{
    return bairro;
}    

void agencia::setbairro(string bairro)
{
    this->bairro = bairro;
}    

string agencia::getestado()
{
    return estado;
}    

void agencia::setestado(string estado)
{
    this->estado = estado;    
}   
 
string agencia::getcep()
{
    return cep;   
}  

void agencia::setcep(string cep)  
{
    this->cep = cep;
}    

string agencia::gettelefone()
{
    return telefone;
}    

void agencia::settelefone(string telefone)
{
    this->telefone = telefone;
}    

void agencia::mostraagencia()
{
    cout<<"  #-#-# -> Nome: "<<banco;
    cout<<endl;
    cout<<"  #-#-# -> Endereco: "<<endereco;
    cout<<endl;
    cout<<"  #-#-# -> Cidade: "<<cidade;
    cout<<endl;
    cout<<"  #-#-# -> Bairro: "<<bairro;
    cout<<endl;
    cout<<"  #-#-# -> Estado: "<<estado;
    cout<<endl;
    cout<<"  #-#-# -> CEP: "<<cep;
    cout<<endl;
    cout<<"  #-#-# -> Telefone: "<<telefone;
    cout<<endl<<endl;
}    

/*
  Fun��es da classe cliente
*/

cliente::~cliente()
{
}    

cliente::cliente()
{    
}

cliente::cliente(string nome, string cpf, string rg, char sexo, string endereco, string bairro, string cidade, string estado, string cep, string telefone, string estado_civil)
{
    this->nome = nome;
    this->cpf = cpf;
    this->rg = rg;
    this->sexo = sexo;
    this->endereco = endereco;
    this->bairro = bairro;
    this->cidade = cidade;
    this->estado = estado;
    this->cep = cep;
    this->telefone = telefone;
    this->estado_civil = estado_civil;
} 

string cliente::getnome()
{
    return nome;
}              

void cliente::setnome(string nome)
{
    this->nome = nome;
}    

string cliente::getcpf()
{
    return cpf;    
}    

void cliente::setcpf(string cpf)
{
    this->cpf = cpf;
}    

string cliente::getrg()
{
    return rg;
}    

void cliente::setrg(string rg)
{
    this->rg = rg;
}    

char cliente::getsexo()
{
    return sexo;
}    

void cliente::setsexo(char sexo)
{
    this->sexo = sexo;
}    

string cliente::getendereco()
{
    return endereco;
}    

void cliente::setendereco(string endereco)
{
    this->endereco = endereco;
}    

string cliente::getbairro()
{
    return bairro;
}    

void cliente::setbairro(string bairro)
{
    this->bairro = bairro;
}    

string cliente::getcidade()
{
    return cidade;
}    

void cliente::setcidade(string cidade)
{
    this->cidade = cidade;
}    

string cliente::getestado()
{
    return estado;
}    

void cliente::setestado(string estado)
{
    this->estado = estado;
}    

string cliente::getcep()
{
    return cep;
}    

void cliente::setcep(string cep)
{
    this->cep = cep;
}    

string cliente::gettelefone()
{
    return telefone;
}    

void cliente::settelefone(string telefone)
{
    this->telefone = telefone;
}    

string cliente::getestado_civil()
{
    return estado_civil;
}    

void cliente::setestado_civil(string estado_civil)
{
    this->estado_civil = estado_civil;
}   

void cliente::mostracliente()
{
        cout<<"  #-#-# -> Nome: "<<nome;            
        cout<<endl;    
        cout<<"  #-#-# -> Cpf: "<<cpf;
        cout<<endl;
        cout<<"  #-#-# -> Rg: "<<rg;
        cout<<endl;   
        cout<<"  #-#-# -> Sexo (M / F): "<<sexo;
        cout<<endl;
        cout<<"  #-#-# -> Endereco: "<<endereco;
        cout<<endl;
        cout<<"  #-#-# -> Bairro: "<<bairro;
        cout<<endl;
        cout<<"  #-#-# -> Cidade: "<<cidade;
        cout<<endl;
        cout<<"  #-#-# -> Estado: "<<estado;	   			        
        cout<<endl;
        cout<<"  #-#-# -> Cep: "<<cep; 			        
        cout<<endl;
        cout<<"  #-#-# -> Telefone: "<<telefone;	        
        cout<<endl;
        cout<<"  #-#-# -> Estado Civil: "<<estado_civil;
        cout<<endl<<endl;
}  
      

/*
  Fun��es da classe conta
*/

conta::~conta()
{
}    

conta::conta()
{    
}

conta::conta(int cliente, int agencia, string tipo, string data_cadastro, string atendente)

{
   this->cliente = cliente;
   this->agencia = agencia;
   this->tipo = tipo;
   this->data_cadastro = data_cadastro;
   this->atendente = atendente;
                    
}

int conta::getclienteconta()
{
        return cliente;
}

void conta::setclienteconta(int cliente)
{
    this->cliente = cliente;
}    

int conta::getagenciaconta()
{
    return agencia;
}    

void conta::setagenciaconta(int agencia)
{
    this->agencia = agencia;        
}        

string conta::gettipo()
{
       return tipo;
}

void conta::settipo(string tipo)
{
     this->tipo = tipo;
}

string conta::getdata_cadastro()
{
       return data_cadastro;
}

void conta::setdata_cadastro(string data_cadastro)
{
     this->data_cadastro = data_cadastro;
}

string conta::getatendente()
{
       return atendente;
}

void conta::setatendente(string atendente)
{
     this->atendente = atendente;
}

void conta::mostraconta()   
{            
        cout<<"  #-#-# -> Agencia: "<<agencia;
        cout<<endl;
        cout<<"  #-#-# -> Tipo: "<<tipo;
        cout<<endl;          
        cout<<"  #-#-# -> Data Cadastro: "<<data_cadastro;
        cout<<endl;
        cout<<"  #-#-# -> Atendente: "<<atendente;      
        cout<<endl<<endl;
}  
